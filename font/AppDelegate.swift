//
//  AppDelegate.swift
//  font
//
//  Created by kawase yu on 2015/12/25.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils
import Parse
import iAd

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, FontListViewDelegate, DetailViewDelegate, ADBannerViewDelegate {

    var window: UIWindow?
    private let rootViewController = UIViewController()
    private let fontListViewController = FontListViewController()
    
    private let navigationController = UINavigationController()
    private let adBannerView = ADBannerView()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // お約束
        UIApplication.sharedApplication().applicationIconBadgeNumber = -1
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window!.backgroundColor = UIColor.whiteColor()
        self.window!.makeKeyAndVisible()
        
        // parse
        Parse.setApplicationId("yfx1AjfWnPAOIIt8U4tZiOdGIECqzI2jxJ6WnEos", clientKey: "ZLg1bnX6v26O8VDlWox3F6yibneoGrqc0qT8GfFN")
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        
        // push
        let types : UIUserNotificationType =
        [UIUserNotificationType.Badge,
            UIUserNotificationType.Alert,
            UIUserNotificationType.Sound]
        let settings : UIUserNotificationSettings = UIUserNotificationSettings(forTypes: types, categories: nil)
        
        application.registerUserNotificationSettings(settings)
        
        // analytics
        let gai = GAI.sharedInstance()
        gai.trackUncaughtExceptions = true;
        gai.dispatchInterval = 20
//        gai.logger.logLevel = .Verbose
        Util.trackScreen("init")
        
        // 画面
        self.window!.rootViewController = rootViewController
        Util.setup(rootViewController)
        
        fontListViewController.setup()
        fontListViewController.reload()
        fontListViewController.delegate = self
        
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.pushViewController(fontListViewController, animated: false)
        rootViewController.view.addSubview(navigationController.view)
        
        Util.trackScreen("all")
        
        Util.activeApp()
        
        if AloeDevice.isForeignDevice(){
            adBannerView.delegate = self
            adBannerView.backgroundColor = UI.MainColor
            adBannerView.frame.origin.y = UI.WindowHeight - 44 - adBannerView.frame.size.height
            rootViewController.view.addSubview(adBannerView)
        }
    
        return true
    }
    
    // MARK: ADBannerViewDelegate
    
    func bannerViewDidLoadAd(banner: ADBannerView!) {
        print("bannerViewDidLoadAd")
    }
    
    // MARK: FontListViewDelegate
    
    func onSelectArticle(font: Font) {
        
        Util.trackEvent("fontDetail", action: font.fontName)
        
        let detailViewController = DetailViewController()
        detailViewController.setup(font)
        navigationController.pushViewController(detailViewController, animated: true)
    }
    
    // MARK: Default
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        print("didRegisterForRemoteNotificationsWithDeviceToken")
        print(deviceToken)
        
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation.saveInBackground()
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("didReceiveRemoteNotification")
        
        PFPush.handlePush(userInfo)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        Util.activeApp()
        UIApplication.sharedApplication().applicationIconBadgeNumber = -1
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

