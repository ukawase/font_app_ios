//
//  UI.swift
//  font
//
//  Created by kawase yu on 2015/12/26.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

class UI: NSObject {
    
    static let BaseColor = AloeColor.fromHex(0xffffff) /* 白 */
    static let MainColor = AloeColor.fromHex(0x95a5a6) /* 灰色 */
    static let AccentColor = AloeColor.fromHex(0x1abc9c) /* 緑 */
    
    static let TextColor = AloeColor.fromHex(0x333333) /* 濃ゆい黒 */
    static let LineColor = AloeColor.fromHex(0xcccccc) /* 適当 */
    static let LightColor = UI.AccentColor /* タップ時のハイライト */
    
    static let BlackLayerOpacity:CGFloat = 0.3 /* 入力のフォーカスの時の背景透過度 */
    static let BgOpacity:CGFloat = 0.9 /* もしくはガラスにするか */
    static let JaFontLineHeight:CGFloat = 1.4 /* あれね */
    
    static let ButtonR:CGFloat = 5
    
    static let WindowWidth = AloeDevice.windowWidth()
    static let WindowHeight = AloeDevice.windowHeight()
    
    
    static func attributedText(lineHeight:CGFloat, kerning:CGFloat, text:String)->NSAttributedString{
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeight
        let attributedText = NSMutableAttributedString(string: text)
        
        let attributes = [
            NSParagraphStyleAttributeName:paragraphStyle
            , NSKernAttributeName: kerning
        ]
        
        attributedText.addAttributes(attributes, range: NSMakeRange(0, attributedText.length))
        
        return NSAttributedString(attributedString: attributedText)
    }
    
}
