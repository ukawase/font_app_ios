//
//  FontFamily.swift
//  font
//
//  Created by kawase yu on 2015/12/25.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit

class FontFamily:NSObject{
    
    let familyName:String
    var fontList:[Font]
    private var isJa:Bool = false
    
    init(familyName:String, var fontNameList:[String], isJa:Bool=false) {
        self.familyName = familyName
        self.isJa = isJa
        fontNameList.sortInPlace({ $0 < $1 })
        
        var fontList:[Font] = []
        for fontName in fontNameList{
            let font = Font(fontName: fontName, familyName: familyName)
            fontList.append(font)
        }
        self.fontList = fontList
        
        super.init()
    }
    
    func mergeFavorite(favoriteFontNameList:[String]){
        for font in fontList{
            font.favorite = favoriteFontNameList.contains(font.fontName)
        }
    }
    
    func reloadFontList(fontList:[Font]){
        self.fontList = fontList
        
    }
    
    func ja(){
        isJa = true
        for font in fontList{
            font.lang = .JA
        }
    }
    
    func containKeyword(keyword:String)->FontFamily?{
        
        var _fontList:[Font] = []
        
        for font in fontList{
            if let _ = font.fontName.rangeOfString(keyword,options: NSStringCompareOptions.CaseInsensitiveSearch){
                _fontList.append(font)
            }
        }
        
        if _fontList.count == 0{
            return nil
        }
        
        let fontFamily = FontFamily(familyName: familyName, fontNameList:[])
        fontFamily.fontList = _fontList
        if fontFamily.isJa{
            fontFamily.ja()
        }
        
        return fontFamily
    }
    
}