//
//  Word.swift
//  font
//
//  Created by kawase yu on 2016/01/01.
//  Copyright © 2016年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

class Word: NSObject {

    private static let noJa = AloeDevice.isForeignDevice()
    
    static func contact()->String{
        return noJa ? "contact" : "お問い合わせ"
    }
    
    static func request()->String{
        return noJa ? "request features" : "次のバージョンへのご要望"
    }
    
    static func onAppstore()->String{
        return noJa ? "(on appstore review)" : "(appstoreのレビューにて)"
    }
    
    static func submit()->String{
        return noJa ? "save" : "適用"
    }
    
    static func padding()->String{
        return noJa ? "padding" : "余白"
    }
    
    static func done()->String{
        return noJa ? "done" : "完了"
    }
    
    static func title()->String{
        return noJa ? "title" : "タイトル"
    }
    
    static func text()->String{
        return noJa ? "text" : "テキスト"
    }
    
    static func reset()->String{
        return noJa ? "reset" : "リセット"
    }
    
    static func experience()->String{
        return noJa ? "Experience What about?" : "使い心地はどうですか？"
    }
    
    static func pleaseReview()->String{
        return noJa ? "Can you give me a review?" : "ご要望等ありましたらぜひレビューを。"
    }
    
    static func writeReview()->String{
        return noJa ? "OK" : "書いてあげる"
    }
    
    static func cancelReview()->String{
        return noJa ? "no thanks" : "書かない"
    }
    
    static func licenseInfo()->String{
        return noJa ? "This app has been built using the following open source software" : "このアプリは、次のオープンソースソフトウェアを使用して構築されています"
    }
    
}
