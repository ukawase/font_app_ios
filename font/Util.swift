//
//  Util.swift
//  font
//
//  Created by kawase yu on 2015/12/28.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit

enum FontTutori{
    case Kerning, LineHeight
    
    func key()->String{
        switch self{
        case Kerning:
            return "TUTORI_KERNING"
        case .LineHeight:
            return "TUTORI_LINEHEIGHT"
        }
    }
}

enum UserSetting{
    case LargeText, ContentText, Padding
    
    func key()->String{
        switch self{
        case .LargeText:
            return "USER_SETTING_LARGETEXT"
        case .ContentText:
            return "USER_SETTING_CONTENTTEXT"
        case .Padding:
            return "USER_SETTING_PADDING"
        }
    }
}

class Util: NSObject {

    static private var _rootViewController:UIViewController!
    
    static private let UD_FAVORITE_LIST_KEY = "UD_FAVORITE_LIST_KEY"
    static private let UD_LARGE_TEXT_KEY = "UD_LARGE_TEXT_KEY"
    static private let UD_CONTENT_TEXT_KEY = "UD_CONTENT_TEXT_KEY"
    static private let UD_PADDING_KEY = "UD_PADDING_KEY"
    static private let UD_ACTIVE_APP_COUNT_KEY = "UD_ACTIVE_APP_COUNT_KEY"
    static private let UD_EVALUTIONED_REVIEW_KEY = "UD_EVALUTIONED_REVIEW_KEY"
    
    static func setup(rootViewController:UIViewController){
        _rootViewController = rootViewController
    }
    
    static func rootViewController()->UIViewController{
        return _rootViewController
    }
    
    static func rootView()->UIView{
        return rootViewController().view
    }
    
    
    static func article(font:Font)->Article{
        if font.lang == .EN{
            return Article.enArticle()
        }else{
            return Article.userArticle()
//            return Article.jaArticle()
        }
    }
    
    static func favoriteFontNameList()->[String]{
        if let list = NSUserDefaults.standardUserDefaults().arrayForKey(UD_FAVORITE_LIST_KEY) as? [String]{
            return list
        }
        
        return [String]()
    }
    
    static func addFavorite(font:Font){
        var list = favoriteFontNameList()
        if list.contains(font.fontName){
            return
        }
        
        list.append(font.fontName)
        saveFavoriteList(list)
    }
    
    private static func saveFavoriteList(list:[String]){
        let array = NSArray(array: list)
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setObject(array, forKey: UD_FAVORITE_LIST_KEY)
        ud.synchronize()
    }
    
    static func removeFavorite(font:Font){
        var list = favoriteFontNameList()
        if !list.contains(font.fontName){
            return
        }
        
        if let index = list.indexOf(font.fontName){
            list.removeAtIndex(index)
            saveFavoriteList(list)
        }
    }
    
    static func isTutoriEnd(type:FontTutori)->Bool{
        return NSUserDefaults.standardUserDefaults().boolForKey(type.key())
    }
    
    static func endTutori(type:FontTutori){
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setBool(true, forKey: type.key())
        ud.synchronize()
    }
    
    private static let GA_TRACKING_ID = "UA-71892394-1"
    
    static func trackScreen(screenName:String){
        let gai = GAI.sharedInstance()
        let tracker = gai.trackerWithTrackingId(GA_TRACKING_ID)
        let build = GAIDictionaryBuilder.createScreenView().set(screenName, forKey: kGAIScreenName).build() as [NSObject : AnyObject]
        tracker.send(build)
    }
    
    static func trackEvent(category:String, action:String){
        let gai = GAI.sharedInstance()
        let tracker = gai.trackerWithTrackingId(GA_TRACKING_ID)
        
        let build = GAIDictionaryBuilder.createEventWithCategory(category, action: action, label: "", value:1).build() as [NSObject : AnyObject]
        tracker.send(build)
    }
    
    static func saveUserSettingString(type:UserSetting, val:String){
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setObject(val, forKey: type.key())
        ud.synchronize()
    }
    
    static func saveUserSettingInt(type:UserSetting, val:Int){
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setInteger(val, forKey: type.key())
        ud.synchronize()
    }
    
    static func userSettingString(type:UserSetting)->String?{
        return NSUserDefaults.standardUserDefaults().stringForKey(type.key())
    }
    
    static func userSettingInt(type:UserSetting)->Int{
        if userSettingString(.LargeText) != nil{
            return NSUserDefaults.standardUserDefaults().integerForKey(type.key())
        }
        
        return 10
    }
    
    static func activeApp(){
        let ud = NSUserDefaults.standardUserDefaults()
        var current = ud.integerForKey(UD_ACTIVE_APP_COUNT_KEY)
        current++
        ud.setInteger(current, forKey: UD_ACTIVE_APP_COUNT_KEY)
        ud.synchronize()
        
        trackEvent("起動回数", action: "\(current)")
    }
    
    static func activeAppCount()->Int{
        return NSUserDefaults.standardUserDefaults().integerForKey(UD_ACTIVE_APP_COUNT_KEY)
    }
    
    static func evalutionedReview(){
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setBool(true, forKey: UD_EVALUTIONED_REVIEW_KEY)
        ud.synchronize()
    }
    
    static func isEvalutionedReview()->Bool{
        return NSUserDefaults.standardUserDefaults().boolForKey(UD_EVALUTIONED_REVIEW_KEY)
    }
    
    static func showReview()->Bool{
        return (!isEvalutionedReview() && activeAppCount() > 2)
    }
}


