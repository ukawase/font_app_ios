//
//  AloeFont.swift
//  font
//
//  Created by kawase yu on 2015/12/25.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

public class AloeFont: NSObject {
    
    private class AloeFontDownloader{
        
        private let fontName:String
        
        init(fontName:String){
            self.fontName = fontName
        }
        
        func download(callback:(fontName:String)->(), fail:(fontName:String)->()){
             let desc = UIFontDescriptor(name:fontName, size:24)
            
            var isFfail = false
            
            CTFontDescriptorMatchFontDescriptorsWithProgressHandler([desc], nil) { (state:CTFontDescriptorMatchingState, prog:CFDictionary!) -> Bool in
                
                switch state{
//                case .DidBegin:
//                    print("didBegin")
//                case .DidMatch:
//                    print("didMatch")
//                case .Stalled:
//                    print("didStalled")
//                case .Downloading:
//                    print("downloading")
//                    let d = prog as NSDictionary
//                    let key = kCTFontDescriptorMatchingPercentage
//                    let cur : AnyObject? = d[key as NSString]
//                    if let cur = cur as? NSNumber {
//                        NSLog("progress: %@%%", cur)
//                    }
//                case .DidFinishDownloading:
//                    print("DidFinishDownloading")
                case .DidFinish:
                    if !isFfail{
//                        print("DidFinish")
                        let font = UIFont(name: self.fontName, size: 24)
                        callback(fontName: self.fontName)
                    }
                case .DidFailWithError:
                    isFfail = true
//                    print("DidFailWithError")
                    fail(fontName: self.fontName)
                    return false
                    
                default: break
                    
                }
                
                return true
            }
        }
    }
    
    public static let JA_FONT_FAMILY_LIST = [
        "YuGothic"
        , "YuMincho +36p Kana"
        , "YuMincho"
        , "Hiragino Kaku Gothic Std"
        , "Hiragino Kaku Gothic StdN"
        , "Hiragino Sans"
        , "Hiragino Mincho ProN"
        , "Hiragino Maru Gothic Pro"
        , "Hiragino Maru Gothic ProN"
        
        // 埋め込み
        , "IPAexGothic"
        , "IPAexMincho"
        , "M+ 1p"
        , "M+ 2p"
        , "Rounded M+ 1p"
        , "Rounded M+ 2p"
        , "BokutachinoGothic2Bold"
        , "BokutachinoGothic2Regular"
        , "Aozora Mincho"
        , "KokoroMinchoutai"
        , "HannariMincho"
    ]
    
    public static let DOWNLOADABLE_JA_FONT_NAME_LIST = [
        "YuGo-Bold"
        , "YuGo-Medium"
        , "YuMin-Medium"
        , "YuMin-Demibold"
        , "YuMin_36pKn-Medium"
        , "YuMin_36pKn-Demibold"
        , "HiraKakuStd-W8"
        , "HiraKakuStdN-W8"
        , "HiraginoSans-W3"
        , "HiraKakuProN-W3"
        , "HiraginoSans-W6"
        , "HiraKakuProN-W6"
        , "HiraMinProN-W3"
        , "HiraMinProN-W6"
        , "HiraMaruPro-W4"
        , "HiraMaruProN-W4"
    ]
    
    public static func useFont(fontNameList:[String], callback:()->()){
        
        var total = fontNameList.count
        var loaded:Int = 0
        
        func onLoaded(fontName:String){
            loaded++
            if loaded == total{
                AloeThread.mainThread({ () -> () in
                    callback()
                })
            }
        }
        
        for i in 0..<fontNameList.count{
            let fontName = fontNameList[i]
            let downloader = AloeFontDownloader(fontName: fontName)
            AloeThread.subThread({ () -> () in
                downloader.download(onLoaded, fail: onLoaded)
            })
        }
    }
    
    public static func downloadableFontList() -> [(fontName:String, language:String)] {
        let downloadableDescriptor = CTFontDescriptorCreateWithAttributes([
            (kCTFontDownloadableAttribute as NSString): kCFBooleanTrue
            ])
        
        
        guard let cfMatchedDescriptors = CTFontDescriptorCreateMatchingFontDescriptors(downloadableDescriptor, nil), matchedDescriptors = (cfMatchedDescriptors as NSArray) as? [CTFontDescriptor] else {
            return []
        }
        
        var result:[(fontName:String, language:String)] = []
        for descriptor in matchedDescriptors{
            let attributes = CTFontDescriptorCopyAttributes(descriptor) as NSDictionary
            if let fontName = attributes[kCTFontNameAttribute as String] as? String{
                if let languages = attributes["NSCTFontDesignLanguagesAttribute"] as? NSArray{
                    if languages.count > 0{
                        result.append((fontName:fontName, language:languages[0] as! String))
                    }
                }
            }
        }
        
        return result
    }
    
    public static func downloadableJaFontNameList()->[String]{
        var result:[String] = []
        let all = downloadableFontList()
        for row in all{
            if row.language == "ja"{
                result.append(row.fontName)
            }
        }
        
        return result
    }
    
}
