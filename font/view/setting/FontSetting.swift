//
//  FontSetting.swift
//  font
//
//  Created by kawase yu on 2016/01/02.
//  Copyright © 2016年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

protocol FontSettingDelegate:class{
    
    func onShowSetting()
    func onHideSetting()
    func onChangeSetting()
    
}

class FontSetting: NSObject, SettingViewDelegate {
    
    let settingButton = UIButton(frame: CGRectMake(0, 0, 44, 44))
    weak var delegate:FontSettingDelegate!
    private let settingButtonWrapper = UIView(frame: CGRectMake(0, UI.WindowHeight-44, UI.WindowWidth, 44))
    private var currentButtonX:CGFloat = 0
    private let view = UIView(frame: AloeDevice.windowFrame())
    private let contentView = UIView(frame: CGRectMake(UI.WindowWidth, 0, UI.WindowWidth, UI.WindowHeight))
    private let blackLayer = UIView(frame: AloeDevice.windowFrame())
    private var buttonParentView:UIView!
    
    private let settingViewController = SettingViewController()
    
    override init() {
        super.init()
        
        blackLayer.backgroundColor = UIColor.blackColor()
        blackLayer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapBlack"))
        view.addSubview(blackLayer)
        
        contentView.backgroundColor = UI.BaseColor
        view.addSubview(contentView)
        
        view.addSubview(settingButtonWrapper)
        
        settingButton.setImage(UIImage(named: "setting"), forState: .Normal)
        settingButton.setImage(UIImage(named: "xButton"), forState: .Selected)
        settingButton.addTarget(self, action: "tapInfo", forControlEvents: .TouchUpInside)
        settingButton.adjustsImageWhenHighlighted = false
        
        settingViewController.setup()
        settingViewController.delegate = self
        contentView.addSubview(settingViewController.view)
    }
    
    // MARK: public
    
    func reload(){
        
    }
    
    // MARK: private
    
    private dynamic func tapInfo(){
        if !settingButton.selected{
            show()
        }else{
            hide()
        }
    }
    
    private func contentWidth()->CGFloat{
        return UI.WindowWidth
    }
    
    private func hide(){
        delegate.onHideSetting()
        
        let rootView = Util.rootView()
        rootView.addSubview(view)
        
        let fromX = -contentWidth()
        AloeChain().add(0.3, ease: .InSine) { (val) -> () in
            self.blackLayer.alpha = UI.BlackLayerOpacity * ( 1 - val)
            self.contentView.transform = CGAffineTransformMakeTranslation(fromX * (1 - val), 0)
            }.call({ () -> () in
                self.buttonParentView.insertSubview(self.settingButton, atIndex: 1)
                self.view.removeFromSuperview()
            }).execute()
        
        AloeChain().add(0.15, ease: .InBack) { (val) -> () in
            let scale = 1.0 * (1 - val)
            self.settingButton.transform = CGAffineTransformMakeScale(scale, scale)
            }.call({ () -> () in
                self.settingButton.selected = !self.settingButton.selected
            }).add(0.15, ease: .OutBack, progress: { (val) -> () in
                let scale = val
                self.settingButton.transform = CGAffineTransformMakeScale(scale, scale)
            }).execute()
    }
    
    private dynamic func tapBlack(){
        hide()
    }
    
    private func show(){
        
        Util.trackScreen("setting")
        settingViewController.reload()
        buttonParentView = settingButton.superview
        settingButtonWrapper.addSubview(settingButton)
        
        delegate.onShowSetting()
        
        contentView.transform = CGAffineTransformMakeTranslation(UI.WindowWidth, 0)
        blackLayer.alpha = 0
        
        let rootView = Util.rootView()
        rootView.addSubview(view)
        
        let toX = -contentWidth()
        AloeChain().add(0.3, ease: .OutSine) { (val) -> () in
            self.blackLayer.alpha = UI.BlackLayerOpacity * val
            self.contentView.transform = CGAffineTransformMakeTranslation(toX * val, 0)
            }.execute()
        
        AloeChain().add(0.15, ease: .InBack) { (val) -> () in
            let scale = 1.0 * (1 - val)
            self.settingButton.transform = CGAffineTransformMakeScale(scale, scale)
            }.call({ () -> () in
                self.settingButton.selected = !self.settingButton.selected
            }).add(0.15, ease: .OutBack, progress: { (val) -> () in
                let scale = val
                self.settingButton.transform = CGAffineTransformMakeScale(scale, scale)
            }).execute()
    }
    
    // MARK: SettingViewDelegate
    
    func onChangeSetting() {
        delegate.onChangeSetting()
    }
    
    func onSubmit() {
        hide()
    }
}
