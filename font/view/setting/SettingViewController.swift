//
//  SettingViewController.swift
//  font
//
//  Created by kawase yu on 2016/01/02.
//  Copyright © 2016年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

protocol SettingViewDelegate:class{
    
    func onChangeSetting()
    func onSubmit()
    
}

class SettingViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    weak var delegate:SettingViewDelegate!
    private let sv = UIScrollView(frame: CGRectMake(0, 64, UI.WindowWidth, UI.WindowHeight-64))
    private let paddingView = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 44))
    private let paddingLabel = UILabel(frame: CGRectMake(UI.WindowWidth-100-10, 0, 100, 44))
    private let paddingPicker = UIPickerView()
    private let pickerWrapper = UIView()
    
    private let largeTextView = UIView(frame: CGRectMake(0, 44, UI.WindowWidth, 44))
    private let largeTextTf = UITextField(frame: CGRectMake(0, 0, UI.WindowWidth-100-10, 44))
    
    private let contentTextViewWrapper = UIView(frame: CGRectMake(0, 88, UI.WindowWidth, 200))
    private let contentTextView = UITextView(frame: CGRectMake(0, 0, UI.WindowWidth-20, 160))
    
    private let submitButton = UILabel()
    private let submitCheck = UIImageView(image: UIImage(named: "check")!)
    
    private let resetButton = UILabel()
    private let resetCheck = UIImageView(image: UIImage(named: "check")!)
    
    
    private let blackLayer = UIView(frame: AloeDevice.windowFrame())
    
    // MARK: public
    
    func setup(){
        view.backgroundColor = UIColor.whiteColor()
        setHeader()
        
        view.addSubview(sv)
        setPaddingView()
        setLargeTextView()
        setTextView()
        setFormButton()
        
        blackLayer.backgroundColor = UIColor.blackColor()
        blackLayer.alpha = 0
        blackLayer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapBlack"))
    }
    
    // MARK: public
    
    func reload(){
        let padding = Util.userSettingInt(.Padding)
        paddingPicker.selectRow(padding/5, inComponent: 0, animated: false)
        paddingLabel.text = "\(padding)"
        largeTextTf.text = Article.userArticle().title
        contentTextView.text = Article.userArticle().content
        
        submitButton.text = Word.submit()
        submitCheck.transform = CGAffineTransformMakeScale(0, 0)
        
        resetButton.text = Word.reset()
        resetCheck.transform = CGAffineTransformMakeScale(0, 0)
    }
    
    // MARK: private
    
    private func setPaddingView(){
        sv.addSubview(paddingView)
        
        paddingView.backgroundColor = UI.BaseColor
        paddingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapPadding"))
        let label = UILabel(frame: CGRectMake(10, 0, 200, 44))
        label.font = UIFont.systemFontOfSize(14.0)
        label.textColor = UI.TextColor
        label.text = Word.padding()
        paddingView.addSubview(label)
        
        paddingLabel.font = UIFont.boldSystemFontOfSize(14.0)
        paddingLabel.textColor = UI.MainColor
        paddingLabel.textAlignment = .Right
        
        paddingView.addSubview(paddingLabel)
        
        let line = UIView(frame: CGRectMake(10, 43.5, UI.WindowWidth-20, 0.5))
        line.backgroundColor = UI.LineColor
        paddingView.addSubview(line)
        
        paddingPicker.delegate = self
        paddingPicker.dataSource = self
        paddingPicker.showsSelectionIndicator = true
        paddingPicker.frame.origin.y = 44
        
        let bg = UIView(frame: CGRectMake(0, 44, UI.WindowWidth, paddingPicker.frame.size.height))
        bg.backgroundColor = UIColor.whiteColor()
        pickerWrapper.addSubview(bg)
        
        pickerWrapper.addSubview(paddingPicker)
        let av = accessoryView(1)
        let avLine = UIView(frame: CGRectMake(0, 43.5, UI.WindowWidth, 0.5))
        avLine.backgroundColor = UI.LineColor
        av.addSubview(avLine)
        pickerWrapper.addSubview(av)
        pickerWrapper.frame = CGRectMake(0, UI.WindowHeight, UI.WindowWidth, paddingPicker.frame.size.height + 44)
        pickerWrapper.frame.origin.y = UI.WindowHeight
    
        let pickerLine = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 0.5))
        pickerLine.backgroundColor = UI.LineColor
        pickerWrapper.addSubview(pickerLine)
    }
    
    private func accessoryView(tag:Int)->UIView{
        let view = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 44))
        let bg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 44))
        bg.backgroundColor = UIColor.whiteColor()
        bg.alpha = 0.8
        view.addSubview(bg)
        
        let doneButton = UILabel(frame: CGRectMake(UI.WindowWidth-80-5, 5, 80, 34))
        doneButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapDone:"))
        doneButton.backgroundColor = UI.AccentColor
        doneButton.font = UIFont.systemFontOfSize(16.0)
        doneButton.textAlignment = .Center
        doneButton.textColor = UI.BaseColor
        doneButton.layer.cornerRadius = UI.ButtonR
        doneButton.clipsToBounds = true
        doneButton.text = Word.done()
        doneButton.tag = tag
        doneButton.userInteractionEnabled = true
        view.addSubview(doneButton)
        
        return view
    }
    
    private dynamic func tapDone(reco:UITapGestureRecognizer){
        let tag = reco.view!.tag
        
        print("tapDone:\(tag)")
        
        // padding
        if tag == 1{
            hideBlack()
            resetViewPosition()
        }
        // largeText
        else if tag == 2{
            largeTextTf.resignFirstResponder()
        }
        // contentText
        else{
            contentTextView.resignFirstResponder()
        }
    }
    
    private func showPicker(){
        Util.rootView().addSubview(pickerWrapper)
        AloeTween.doTween(0.35, ease: .OutCirc) { (val) -> () in
            self.pickerWrapper.transform = CGAffineTransformMakeTranslation(0, -self.pickerWrapper.frame.size.height*val)
        }
    }
    
    private func hidePicker(){
        if pickerWrapper.superview == nil{
            return
        }
        
        AloeChain().add(0.2, ease: .None) { (val) -> () in
            self.pickerWrapper.transform = CGAffineTransformMakeTranslation(0, -self.pickerWrapper.frame.size.height * (1-val))
        }.call { () -> () in
            self.pickerWrapper.removeFromSuperview()
        }.execute()
    }
    
    private dynamic func tapPadding(){
        showBlack()
        paddingView.transform = CGAffineTransformMakeTranslation(0, 64)
        Util.rootView().addSubview(paddingView)
        showPicker()
    }
    
    private func setLargeTextView(){
        largeTextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapLargeTextView"))
        sv.addSubview(largeTextView)
        
        largeTextView.backgroundColor = UI.BaseColor
        let label = UILabel(frame: CGRectMake(10, 10, 100, 20))
        label.textColor = UI.TextColor
        label.font = UIFont.systemFontOfSize(14.0)
        label.text = Word.title()
        largeTextView.addSubview(label)
        
        let tfWrapper = UIView(frame: CGRectMake(100, 0, UI.WindowWidth-100-10, 44))
        tfWrapper.addSubview(largeTextTf)
        largeTextTf.delegate = self
        largeTextTf.returnKeyType = .Done
        largeTextTf.font = UIFont.boldSystemFontOfSize(14.0)
        largeTextTf.textColor = UI.MainColor
        largeTextTf.textAlignment = .Right
        largeTextTf.userInteractionEnabled = false
        largeTextTf.inputAccessoryView = accessoryView(2)
        largeTextView.addSubview(tfWrapper)
        
        let line = UIView(frame: CGRectMake(10, 43.5, UI.WindowWidth-20, 0.5))
        line.backgroundColor = UI.LineColor
        largeTextView.addSubview(line)
    }
    
    dynamic private func tapLargeTextView(){
        print("tap")
        largeTextTf.userInteractionEnabled = true
        largeTextTf.becomeFirstResponder()
        
        largeTextView.transform = CGAffineTransformMakeTranslation(0, 64)
        Util.rootView().addSubview(largeTextView)
    }
    
    private func setTextView(){
        sv.addSubview(contentTextViewWrapper)
        contentTextViewWrapper.backgroundColor = UI.BaseColor
        
        let label = UILabel(frame: CGRectMake(10, 10, 100, 20))
        label.textColor = UI.TextColor
        label.font = UIFont.systemFontOfSize(14.0)
        label.text = Word.text()
        contentTextViewWrapper.addSubview(label)
        
        let tvWrapper = UIView(frame: CGRectMake(5, 30, UI.WindowWidth-10, 170))
        tvWrapper.addSubview(contentTextView)
        contentTextViewWrapper.addSubview(tvWrapper)
        
        contentTextView.font = UIFont.boldSystemFontOfSize(14.0)
        contentTextView.textColor = UI.MainColor
        contentTextView.backgroundColor = UIColor.clearColor()
        contentTextView.delegate = self
        contentTextView.inputAccessoryView = accessoryView(3)
        
        let line = UIView(frame: CGRectMake(10, 199.5, UI.WindowWidth-20, 0.5))
        line.backgroundColor = UI.LineColor
        contentTextViewWrapper.addSubview(line)
    }
    
    private func setFormButton(){
        let y:CGFloat = 288 + 20
        let buttonWidth = (UI.WindowWidth - 40) / 2
        
        submitButton.frame = CGRectMake(UI.WindowWidth-buttonWidth-10, y, buttonWidth, 44)
        submitButton.backgroundColor = UI.AccentColor
        submitButton.layer.cornerRadius = UI.ButtonR
        submitButton.clipsToBounds = true
        submitButton.userInteractionEnabled = true
        submitButton.textAlignment = .Center
        submitButton.font = UIFont.systemFontOfSize(16.0)
        submitButton.textColor = UI.BaseColor
        submitButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapSubmit:"))
        sv.addSubview(submitButton)
        
        submitCheck.frame = CGRectMake((buttonWidth-22) / 2, 10, 24, 24)
        submitCheck.userInteractionEnabled = false
        submitCheck.transform = CGAffineTransformMakeScale(0, 0)
        submitButton.addSubview(submitCheck)
        
        resetButton.frame = CGRectMake(10, y, buttonWidth, 44)
        resetButton.backgroundColor = UI.MainColor
        resetButton.layer.cornerRadius = UI.ButtonR
        resetButton.clipsToBounds = true
        resetButton.userInteractionEnabled = true
        resetButton.textAlignment = .Center
        resetButton.textColor = UI.BaseColor
        resetButton.font = UIFont.systemFontOfSize(16.0)
        resetButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapReset:"))
        sv.addSubview(resetButton)
        
        resetCheck.frame = CGRectMake((buttonWidth-22) / 2, 10, 24, 24)
        resetCheck.userInteractionEnabled = false
        resetCheck.transform = CGAffineTransformMakeScale(0, 0)
        resetButton.addSubview(resetCheck)
    }
    
    private dynamic func tapSubmit(reco:UITapGestureRecognizer){
        
        Util.saveUserSettingString(.LargeText, val: largeTextTf.text!)
        Util.saveUserSettingString(.ContentText, val: contentTextView.text)
        Util.saveUserSettingInt(.Padding, val: paddingPicker.selectedRowInComponent(0)*5)
        
        Article.clear()
        delegate.onChangeSetting()
        
        let submitLabel = reco.view as! UILabel
        
        AloeChain().add(0.1, ease: .None) { (val) -> () in
            let scale = 1 - (0.03 * val)
            submitLabel.transform = CGAffineTransformMakeScale(scale, scale)
        }.call({ () -> () in
            submitLabel.text = ""
        }).add(0.1, ease: .OutBack) { (val) -> () in
            let scale = 0.97 + (0.03 * val)
            submitLabel.transform = CGAffineTransformMakeScale(scale, scale)
            self.submitCheck.transform = CGAffineTransformMakeScale(val, val)
        }.wait(0.3).call({ () -> () in
            self.delegate.onSubmit()
        }).execute()
    }
    
    private dynamic func tapReset(reco:UITapGestureRecognizer){
        let view = reco.view!
        paddingPicker.selectRow(2, inComponent: 0, animated: false)
        paddingLabel.text = "10"
        
        if AloeDevice.isForeignDevice(){
            largeTextTf.text = Article.enArticle().title
            contentTextView.text = Article.enArticle().content
        }else{
            largeTextTf.text = Article.jaArticle().title
            contentTextView.text = Article.jaArticle().content
        }
        
        AloeChain().add(0.1, ease: .None) { (val) -> () in
            let scale = 1 - (0.03 * val)
            view.transform = CGAffineTransformMakeScale(scale, scale)
        }.call({ () -> () in
            self.resetButton.text = ""
        }).add(0.1, ease: .OutBack) { (val) -> () in
            let scale = 0.97 + (0.03 * val)
            view.transform = CGAffineTransformMakeScale(scale, scale)
            self.resetCheck.transform = CGAffineTransformMakeScale(val, val)
        }.wait(0.6).add(0.2, ease: .None, progress: { (val) -> () in
            self.resetCheck.transform = CGAffineTransformMakeScale(1-val, 1-val)
        }).call({ () -> () in
            self.resetButton.text = Word.reset()
        }).execute()
    }
    
    private func setHeader(){
        let headerView = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 64))
        view.addSubview(headerView)
        
        let bg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 64))
        bg.backgroundColor = UI.BaseColor
        bg.alpha = UI.BgOpacity
        headerView.addSubview(bg)
        
        let line = UIView(frame: CGRectMake(0, 63, UI.WindowWidth, 1))
        line.backgroundColor = UI.LineColor
        headerView.addSubview(line)
        
        let titleLabel = UILabel(frame: CGRectMake(0, 20, UI.WindowWidth, 44))
        titleLabel.font = UIFont.systemFontOfSize(16.0)
        titleLabel.textColor = UI.TextColor
        titleLabel.text = "setting"
        titleLabel.textAlignment = .Center
        headerView.addSubview(titleLabel)
    }
    
    private func showBlack(){
        blackLayer.alpha = 0
        Util.rootView().addSubview(blackLayer)
        
        AloeChain().add(0.2, ease: .None) { (val) -> () in
            self.blackLayer.alpha = UI.BlackLayerOpacity * val
        }.execute()
    }
    
    private func hideBlack(){
        AloeChain().add(0.2, ease: .None) { (val) -> () in
            self.blackLayer.alpha = UI.BlackLayerOpacity * (1 - val)
        }.call({ () -> () in
            self.blackLayer.removeFromSuperview()
        }).execute()
    }
    
    dynamic private func tapBlack(){
        hideBlack()
        resetViewPosition()
    }
    
    private func resetViewPosition(){
        paddingView.transform = CGAffineTransformMakeTranslation(0, 0)
        sv.addSubview(paddingView)
        hidePicker()
        
        largeTextTf.resignFirstResponder()
        largeTextView.transform = CGAffineTransformMakeTranslation(0, 0)
        sv.addSubview(largeTextView)
        
        contentTextView.resignFirstResponder()
        sv.setContentOffset(CGPointMake(0, 0), animated: true)
        contentTextViewWrapper.transform = CGAffineTransformMakeTranslation(0, 0)
        sv.addSubview(contentTextViewWrapper)
    }
    
    private func paddingVal(row:Int)->Int{
        return row * 5
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        showBlack()
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        largeTextTf.userInteractionEnabled = false
        hideBlack()
        resetViewPosition()
        
        return true
    }
    
    // MARK: UITextViewDelegate
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        showBlack()
        
        contentTextViewWrapper.transform = CGAffineTransformMakeTranslation(0, -24)
        Util.rootView().addSubview(contentTextViewWrapper)
        sv.setContentOffset(CGPointMake(0, 88), animated: true)
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        hideBlack()
        resetViewPosition()
    }
    
    // MARK: UIPickerViewDelegate, Datasource
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let padding = paddingVal(row)
        paddingLabel.text = "\(padding)"
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 11
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row*5)"
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // MARK: default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        view.frame.origin.y = 0
        view.frame.size.height = UI.WindowHeight
    }

}
