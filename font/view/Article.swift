//
//  Article.swift
//  font
//
//  Created by kawase yu on 2015/12/26.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

class Article: NSObject {

    let title:String
    let excerpt:String
    let content:String
    
    init(title:String, excerpt:String, content:String) {
        self.title = title
        self.excerpt = excerpt
        self.content = content
    }
    
    func clone()->Article{
        return Article(title: title, excerpt: excerpt, content: content)
    }
    
    private static var _jaArticle:Article?
    private static var _enArticle:Article?
    private static var _userArticle:Article?
    
    static func clear(){
        _jaArticle = nil
        _enArticle = nil
        _userArticle = nil
    }
    
    static func userArticle()->Article{
        
        if let article = _userArticle{
            return article
        }
        
        if let largeText = Util.userSettingString(.LargeText){
            let contentText = Util.userSettingString(.ContentText)!
            var excerpt = contentText.stringByReplacingOccurrencesOfString("\n", withString: "")
            
            if !AloeDevice.isForeignDevice() && excerpt.characters.count > 100{
                excerpt = (excerpt as NSString).substringToIndex(100)
            }
            
            _userArticle = Article(title: largeText, excerpt: excerpt, content: contentText)
            return _userArticle!
        }
        
        // copy
        if AloeDevice.isForeignDevice(){
            _userArticle = enArticle().clone()
        }else{
            _userArticle = jaArticle().clone()
        }
        
        return _userArticle!
    }
    
    static func jaArticle()->Article{
        if let article = _jaArticle{
            return article
        }
        
        _jaArticle = Article(title: "ポラーノの広場", excerpt: "あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモーリオ市、郊外のぎらぎらひかる草の波。祇辻飴葛蛸鯖鰯噌庖箸", content: "そのころわたくしは、モリーオ市の博物局に勤めて居りました。\n十八等官でしたから役所のなかでも、ずうっと下の方でしたし俸給ほうきゅうもほんのわずかでしたが、受持ちが標本の採集や整理で生れ付き好きなことでしたから、わたくしは毎日ずいぶん愉快にはたらきました。\nABCDEFGHIJKLMabcdefghijklm1234567890\n殊にそのころ、モリーオ市では競馬場を植物園に拵こしらえ直すというので、その景色のいいまわりにアカシヤを植え込んだ広い地面が、切符売場や信号所の建物のついたまま、わたくしどもの役所の方へまわって来たものですから、わたくしはすぐ宿直という名前で月賦で買った小さな蓄音器と二十枚ばかりのレコードをもって、その番小屋にひとり住むことになりました。わたくしはそこの馬を置く場所に板で小さなしきいをつけて一疋の山羊を飼いました。毎朝その乳をしぼってつめたいパンをひたしてたべ、それから黒い革のかばんへすこしの書類や雑誌を入れ、靴もきれいにみがき、並木のポプラの影法師を大股にわたって市の役所へ出て行くのでした。\n\n　あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。\nまたそのなかでいっしょになったたくさんのひとたち、ファゼーロとロザーロ、羊飼のミーロや、顔の赤いこどもたち、地主のテーモ、山猫博士のボーガント・デストゥパーゴなど、いまこの暗い巨きな石の建物のなかで考えていると、みんなむかし風のなつかしい青い幻燈のように思われます。では、わたくしはいつかの小さなみだしをつけながら、しずかにあの年のイーハトーヴォの五月から十月までを書きつけましょう。")
        
        return _jaArticle!
    }
    
    static func enArticle()->Article{
        if let article = _enArticle{
            return article
        }
        
        _enArticle = Article(title: "The Black Caty", excerpt: "For the most wild, yet most homely narrative which I am about to pen, I neither expect nor solicit belief. Mad indeed would I be to expect it in a case where my very senses reject their own evidence.", content: "For the most wild, yet most homely narrative which I am about to pen, I neither expect nor solicit belief. Mad indeed would I be to expect it in a case where my very senses reject their own evidence. Yet mad am I not—and very surely do I not dream. But tomorrow I die, and today I would unburthen my soul. My immediate purpose is to place before the world plainly, succinctly, and without comment, a series of mere household events. In their consequences these events have terrified—have tortured—have destroyed me. Yet I will not attempt to expound them. To me they presented little but horror—to many they will seem less terrible than baroques. Hereafter, perhaps, some intellect may be found which will reduce my phantasm to the commonplace—some intellect more calm, more logical, and far less excitable than my own, which will perceive, in the circumstances I detail with awe, nothing more than an ordinary succession of very natural causes and effects.\n\nFrom my infancy I was noted for the docility and humanity of my disposition. My tenderness of heart was even so conspicuous as to make me the jest of my companions. I was especially fond of animals, and was indulged by my parents with a great variety of pets. With these I spent most of my time, and never was so happy as when feeding and caressing them. This peculiarity of character grew with my growth, and in my manhood I derived from it one of my principal sources of pleasure. To those who have cherished an affection for a faithful and sagacious dog, I need hardly be at the trouble of explaining the nature or the intensity of the gratification thus derivable. There is something in the unselfish and self-sacrificing love of a brute which goes directly to the heart of him who has had frequent occasion to test the paltry friendship and gossamer fidelity of mere Man.\n\nABCDEFGHIJKLMabcdefghijklm1234567890")
        
        return _enArticle!
    }

}
