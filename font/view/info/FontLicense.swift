//
//  FontLicense.swift
//  font
//
//  Created by kawase yu on 2015/12/31.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

class FontLicense: NSObject {

    let name:String!
    let url:String!
    var content:String = ""
    
    init(name:String, url:String, filePath:String){
        self.name = name
        self.url = url
        
        super.init()
        
        AloeThread.subThread { () -> () in
            self.content = try! String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding)
        }
    }
    
    static func ipa()->FontLicense{
        let filePath = NSBundle.mainBundle().pathForResource("IPA_Font_License_Agreement_v1.0", ofType: "txt")!
        return FontLicense(name: "IPAフォント, はんなり明朝, こころ明朝体, あおぞら明朝", url: "http://ipafont.ipa.go.jp/old/ipafont/download.html", filePath: filePath)
    }
    
    static func mplus()->FontLicense{
        let filePath = NSBundle.mainBundle().pathForResource("LICENSE_E", ofType: "txt")!
        return FontLicense(name: "M+ FONTS", url: "https://mplus-fonts.osdn.jp/about-en.html", filePath: filePath)
    }
    
    static func roundedMplus()->FontLicense{
        let filePath = NSBundle.mainBundle().pathForResource("README_E_Rounded", ofType: "txt")!
        return FontLicense(name: "Rounded M+ FONTS", url: "http://jikasei.me/font/rounded-mplus/license.html", filePath: filePath)
    }
    
    static func boku()->FontLicense{
        let filePath = NSBundle.mainBundle().pathForResource("LICENSE", ofType: "txt")!
        return FontLicense(name: "ぼくたちのゴシック2", url: "http://fontopo.com/?p=94", filePath: filePath)
    }
    
}
