//
//  InfoViewController.swift
//  font
//
//  Created by kawase yu on 2015/12/31.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private let appCell = AppCell()
    private let tableView = UITableView(frame: CGRectMake(0, 64, UI.WindowWidth, UI.WindowHeight-64))
    private let licenseInfoSection = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 60))
    private let licenseList:[FontLicense] = [
        FontLicense.mplus()
        , FontLicense.roundedMplus()
        , FontLicense.ipa()
        , FontLicense.boku()
    ]
    
    // MARK: public
    
    func setup(){
        view.backgroundColor = UIColor.whiteColor()
        appCell.setup()
        setHeader()
        createInfoSection()
        
        setTableView()
        
    }
    
    func toTop(){
        tableView.contentOffset.y = 0
    }
    
    // MARK: private
    
    private func createInfoSection(){
        let bg = UIView(frame: CGRectMake(0, 20, UI.WindowWidth, 40))
        bg.backgroundColor = UI.MainColor
        licenseInfoSection.addSubview(bg)
        
        let label = UILabel(frame: CGRectMake(10, 20, UI.WindowWidth-20, 40))
        label.font = UIFont.systemFontOfSize(14.0)
        label.numberOfLines = 2
        label.textColor = UI.BaseColor
        label.attributedText = UI.attributedText(1.0, kerning: 0, text: Word.licenseInfo())
        licenseInfoSection.addSubview(label)
        
        let line = UIView(frame: CGRectMake(0, 59, UI.WindowWidth, 1))
        line.backgroundColor = UI.BaseColor
        licenseInfoSection.addSubview(line)
    }
    
    private func setHeader(){
        let headerView = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 64))
        view.addSubview(headerView)
        
        let bg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 64))
        bg.backgroundColor = UI.BaseColor
        bg.alpha = UI.BgOpacity
        headerView.addSubview(bg)
        
        let line = UIView(frame: CGRectMake(0, 63, UI.WindowWidth, 1))
        line.backgroundColor = UI.LineColor
        headerView.addSubview(line)
        
        let titleLabel = UILabel(frame: CGRectMake(0, 20, UI.WindowWidth, 44))
        titleLabel.font = UIFont.systemFontOfSize(16.0)
        titleLabel.textColor = UI.TextColor
        titleLabel.text = "about"
        titleLabel.textAlignment = .Center
        headerView.addSubview(titleLabel)
    }
    
    private func setTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .None
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0)
        
        view.addSubview(tableView)
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return licenseList.count + 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1{
            return 0
        }
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.section == 0{
            return (44*2) + 64
        }else if indexPath.section == 1{
            return 0
        }
        
        let license = licenseList[indexPath.section-2]
        return LicenseCell.height(license)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            return appCell
        }
        
        
        let cellIdentifier:String = "licenceCellIdentifier"
        var cell:LicenseCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! LicenseCell?
        
        if cell == nil {
            cell = LicenseCell(style:UITableViewCellStyle.Default, reuseIdentifier:cellIdentifier)
            cell!.setup()
        }
        
        let license = licenseList[indexPath.section-2]
        cell!.reload(license)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1{
            return licenseInfoSection
        }
        
        var sectionHeader = tableView.headerViewForSection(section) as! LicenseSection?
        if sectionHeader == nil{
            sectionHeader = LicenseSection()
            sectionHeader!.setup()
        }
        
        if section == 0{
            sectionHeader!.reload("about app")
        }else{
            sectionHeader!.reload(licenseList[section-2].name)
        }
        
        return sectionHeader
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else if section == 1{
            return 60
        }
        return 30
    }
    
    // MARK: default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        view.frame.origin.y = 0
        view.frame.size.height = UI.WindowHeight
    }

}
