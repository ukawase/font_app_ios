//
//  LicenseSection.swift
//  font
//
//  Created by kawase yu on 2015/12/31.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit

class LicenseSection: UITableViewHeaderFooterView {
    
    private let nameLabel = UILabel(frame: CGRectMake(10, 0, UI.WindowWidth-20, 30))
    private let bg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 30))
    
    func setup(){
        bg.backgroundColor = UI.MainColor
        contentView.addSubview(bg)
        
        nameLabel.font = UIFont.systemFontOfSize(14.0)
        nameLabel.textColor = UI.BaseColor
        nameLabel.adjustsFontSizeToFitWidth = true
        contentView.addSubview(nameLabel)
    }
    
    func reload(str:String){
        nameLabel.text = str
    }
    
}