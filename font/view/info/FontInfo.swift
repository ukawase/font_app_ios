//
//  InfoView.swift
//  font
//
//  Created by kawase yu on 2015/12/30.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

protocol FontInfoDelegate:class{
    
    func onShowInfo()
    func onHideInfo()
    
}

class FontInfo: NSObject {

    let infoButton = UIButton(frame: CGRectMake(0, 0, 44, 44))
    weak var delegate:FontInfoDelegate!
    private let infoButtonWrapper = UIView(frame: CGRectMake(0, UI.WindowHeight-44, UI.WindowWidth, 44))
    private var currentButtonX:CGFloat = 0
    private let view = UIView(frame: AloeDevice.windowFrame())
    private let contentView = UIView(frame: CGRectMake(UI.WindowWidth, 0, UI.WindowWidth, UI.WindowHeight))
    private let blackLayer = UIView(frame: AloeDevice.windowFrame())
    private var buttonParentView:UIView!
    
    private let infoViewController = InfoViewController()
    
    override init() {
        super.init()
        
        blackLayer.backgroundColor = UIColor.blackColor()
        blackLayer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapBlack"))
        view.addSubview(blackLayer)
        
        contentView.backgroundColor = UI.BaseColor
        view.addSubview(contentView)
        
        view.addSubview(infoButtonWrapper)
        
        infoButton.setImage(UIImage(named: "info"), forState: .Normal)
        infoButton.setImage(UIImage(named: "xButton"), forState: .Selected)
        infoButton.addTarget(self, action: "tapInfo", forControlEvents: .TouchUpInside)
        infoButton.adjustsImageWhenHighlighted = false
        
        infoViewController.setup()
        contentView.addSubview(infoViewController.view)
    }
    
    private dynamic func tapInfo(){
        if !infoButton.selected{
            show()
        }else{
            hide()
        }
    }
    
    private func contentWidth()->CGFloat{
        return UI.WindowWidth
    }
    
    private func hide(){
        delegate.onHideInfo()
        
        let rootView = Util.rootView()
        rootView.addSubview(view)
        
        let fromX = -contentWidth()
        AloeChain().add(0.3, ease: .InSine) { (val) -> () in
            self.blackLayer.alpha = UI.BlackLayerOpacity * ( 1 - val)
            self.contentView.transform = CGAffineTransformMakeTranslation(fromX * (1 - val), 0)
        }.call({ () -> () in
            self.buttonParentView.insertSubview(self.infoButton, atIndex: 1)
            self.view.removeFromSuperview()
        }).execute()
        
        AloeChain().add(0.15, ease: .InBack) { (val) -> () in
            let scale = 1.0 * (1 - val)
            self.infoButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.call({ () -> () in
            self.infoButton.selected = !self.infoButton.selected
        }).add(0.15, ease: .OutBack, progress: { (val) -> () in
            let scale = val
            self.infoButton.transform = CGAffineTransformMakeScale(scale, scale)
        }).execute()
        
    }
    
    private dynamic func tapBlack(){
        hide()
    }
    
    private func show(){
        
        Util.trackScreen("info")
        infoViewController.toTop()
        buttonParentView = infoButton.superview
        infoButtonWrapper.addSubview(infoButton)
        
        delegate.onShowInfo()
        
        contentView.transform = CGAffineTransformMakeTranslation(UI.WindowWidth, 0)
        blackLayer.alpha = 0
        
        let rootView = Util.rootView()
        rootView.addSubview(view)
        
        let toX = -contentWidth()
        AloeChain().add(0.3, ease: .OutSine) { (val) -> () in
            self.blackLayer.alpha = UI.BlackLayerOpacity * val
            self.contentView.transform = CGAffineTransformMakeTranslation(toX * val, 0)
        }.execute()
        
        AloeChain().add(0.15, ease: .InBack) { (val) -> () in
            let scale = 1.0 * (1 - val)
            self.infoButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.call({ () -> () in
            self.infoButton.selected = !self.infoButton.selected
        }).add(0.15, ease: .OutBack, progress: { (val) -> () in
            let scale = val
            self.infoButton.transform = CGAffineTransformMakeScale(scale, scale)
        }).execute()
        
    }
    
}
