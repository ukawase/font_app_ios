//
//  LicenseCell.swift
//  font
//
//  Created by kawase yu on 2015/12/31.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit

class LicenseCell: UITableViewCell {

    static private let TextLabel = UILabel()
    
    static func height(license:FontLicense)->CGFloat{
        let content = license.content
        
        TextLabel.numberOfLines = 0
        TextLabel.font = UIFont.systemFontOfSize(14.0)
        TextLabel.frame = CGRectMake(10, 10, UI.WindowWidth-20, 10000)
        TextLabel.attributedText = UI.attributedText(1.2, kerning: 0, text: content)
        TextLabel.sizeToFit()
        
        return TextLabel.frame.size.height + 20
    }
    
    
    private let contentLabel = UILabel()
    
    func setup(){
        selectionStyle = .None
        
        contentView.addSubview(contentLabel)
        
        contentLabel.font = UIFont.systemFontOfSize(14.0)
        contentLabel.numberOfLines = 0
    }
    
    func reload(license:FontLicense){
        contentLabel.frame = CGRectMake(10, 10, UI.WindowWidth-20, 10000)
        contentLabel.attributedText = UI.attributedText(1.2, kerning: 0, text: license.content)
        contentLabel.sizeToFit()
    }
    
}
