//
//  AppCell.swift
//  font
//
//  Created by kawase yu on 2015/12/31.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit

class AppCell: UITableViewCell {

    func setup(){
        setVersionView()
        setContactView()
        setReviewViewView()
    }
    
    private func setVersionView(){
        let versionView = UIView(frame: CGRectMake(0, 0,UI.WindowWidth,44))
        contentView.addSubview(versionView)
        
        let label = UILabel(frame: CGRectMake(10, 0, UI.WindowWidth-20, 44))
        label.font = UIFont.systemFontOfSize(14.0)
        label.textColor = UI.TextColor
        label.text = "version"
        versionView.addSubview(label)
        
        let valueLabel = UILabel(frame: CGRectMake(10, 0, UI.WindowWidth-20, 44))
        valueLabel.font = UIFont.boldSystemFontOfSize(14.0)
        valueLabel.textColor = UI.MainColor
        valueLabel.textAlignment = .Right
        valueLabel.text = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as? String
        versionView.addSubview(valueLabel)
        
        let line = UIView(frame: CGRectMake(10, 43.5, UI.WindowWidth-20, 0.5))
        line.backgroundColor = UI.LineColor
        versionView.addSubview(line)
    }
    
    private func setContactView(){
        let versionView = UIView(frame: CGRectMake(0, 44,UI.WindowWidth,44))
        versionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapContact"))
        contentView.addSubview(versionView)
        
        let label = UILabel(frame: CGRectMake(10, 0, UI.WindowWidth-20, 44))
        label.font = UIFont.systemFontOfSize(14.0)
        label.textColor = UI.TextColor
        label.text = Word.contact()
        versionView.addSubview(label)
        
        let arrow = UIImageView(frame: CGRectMake(UI.WindowWidth-9.5-10, (44 - 18) / 2, 9.5, 18))
        arrow.image = UIImage(named: "rightArrow")
        versionView.addSubview(arrow)
        
        let line = UIView(frame: CGRectMake(10, 43.5, UI.WindowWidth-20, 0.5))
        line.backgroundColor = UI.LineColor
        versionView.addSubview(line)
    }
    
    private func setReviewViewView(){
        let versionView = UIView(frame: CGRectMake(0,44*2,UI.WindowWidth,64))
        versionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapReview"))
        contentView.addSubview(versionView)
        
        let label = UILabel(frame: CGRectMake(10, 10, UI.WindowWidth-20, 25))
        label.font = UIFont.systemFontOfSize(14.0)
        label.textColor = UI.TextColor
        label.text = Word.request()
        versionView.addSubview(label)
        
        let label2 = UILabel(frame: CGRectMake(10, 30, UI.WindowWidth-20, 23))
        label2.font = UIFont.systemFontOfSize(12.0)
        label2.textColor = UI.TextColor
        label2.text = Word.onAppstore()
        versionView.addSubview(label2)
        
        let arrow = UIImageView(frame: CGRectMake(UI.WindowWidth-9.5-10, (64 - 18) / 2, 9.5, 18))
        arrow.image = UIImage(named: "rightArrow")
        versionView.addSubview(arrow)
        
        let line = UIView(frame: CGRectMake(10, 63.5, UI.WindowWidth-20, 0.5))
        line.backgroundColor = UI.LineColor
        versionView.addSubview(line)
    }
    
    private dynamic func tapContact(){
        let contactUrl = "http://goo.gl/forms/BcFM59uqfX"
        let url = NSURL(string: contactUrl)
        if UIApplication.sharedApplication().canOpenURL(url!){
            UIApplication.sharedApplication().openURL(url!)
        }
    }
    
    private dynamic func tapReview(){
        let reviewUrl = "https://goo.gl/RACqpf"
        let url = NSURL(string: reviewUrl)
        if UIApplication.sharedApplication().canOpenURL(url!){
            UIApplication.sharedApplication().openURL(url!)
        }
    }

}
