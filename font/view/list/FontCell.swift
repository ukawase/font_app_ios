//
//  FontCell.swift
//  font
//
//  Created by kawase yu on 2015/12/25.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

protocol FontCellDelegate: class{
    func onToggleFavoriteFromCell()
}

class FontCell: UITableViewCell {
    
    static func height(font:Font)->CGFloat{
        
        return 44
    }
    
    weak var delegate:FontCellDelegate!
    private let fontNameLabel = UILabel(frame: CGRectMake(37, 0, UI.WindowWidth-37-10, 44))
    private let lightView = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 44))
    private let potti = UIView(frame: CGRectMake(10, (44-10) / 2, 10, 10))
    private let favoriteButton = UIButton(frame: CGRectMake(7, 0, 44, 44))
    private var _font:Font!
    
    func setup(){
        selectionStyle = .None
        
        lightView.backgroundColor = UI.LightColor
        lightView.alpha = 0
        contentView.addSubview(lightView)
        
        fontNameLabel.textColor = UI.TextColor
        contentView.addSubview(fontNameLabel)
        
        potti.backgroundColor = UI.AccentColor
        potti.layer.cornerRadius = 5
        potti.layer.borderWidth = 2
        potti.clipsToBounds = true
        
        favoriteButton.addSubview(potti)
        favoriteButton.addTarget(self, action: "tapFavorite", forControlEvents: UIControlEvents.TouchUpInside)
        favoriteButton.hidden = true
        contentView.addSubview(favoriteButton)
        
        let line = UIView(frame: CGRectMake(10, 43.5, UI.WindowWidth-20, 0.5))
        line.backgroundColor = UI.LineColor
        contentView.addSubview(line)
    }
    
    private dynamic func tapFavorite(){
        _font.favorite = !_font.favorite
        if _font.favorite{
            Util.addFavorite(_font)
        }else{
            Util.removeFavorite(_font)
        }
        
        delegate.onToggleFavoriteFromCell()
        reloadSelf()
    }
    
    // MARK: public
    
    func light(){
        AloeChain().add(0.1, ease: .None) { (val) -> () in
            self.lightView.alpha = 0.6 * val
        }.wait(0.1).add(0.2, ease: .InCirc) { (val) -> () in
            self.lightView.alpha = 0.6 * (1 - val)
        }.execute()
    }
    
    func reload(font:Font){
//        fontNameLabel.font = font.font(16.0)
        self._font = font
        
        if font.favorite{
            potti.backgroundColor = UI.AccentColor
            potti.layer.borderColor = UI.AccentColor.CGColor
        }else{
            potti.backgroundColor = UIColor.clearColor()
            potti.layer.borderColor = UI.MainColor.CGColor
        }
        
        fontNameLabel.font = UIFont.systemFontOfSize(16.0)
        fontNameLabel.text = font.fontName
    }
    
    func reloadSelf(){
        reload(_font)
    }
    
    func focus(){
        favoriteButton.hidden = false
    }
    
    func blur(){
        favoriteButton.hidden = true
    }

}
