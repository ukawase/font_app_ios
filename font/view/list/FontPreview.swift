//
//  FontPreview.swift
//  font
//
//  Created by kawase yu on 2015/12/29.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit

protocol FontPreviewDelegate:class{
    
    func onToggleFavorite()
    
}

class FontPreview: NSObject {

    weak var delegate:FontPreviewDelegate!
    let view = UIView(frame: CGRectMake(0, 20, UI.WindowWidth, 200))
    private var font:Font!
    private let titleLabel = UILabel()
    private let excerptLabel = UILabel()
    private let fontNameLabel = UILabel(frame: CGRectMake(10, 170, UI.WindowWidth-20, 20))
    private let favoriteButton = UIButton(frame: CGRectMake(10, 200-44-10, 44, 44))
    
    override init() {
        super.init()
        view.clipsToBounds = true
    
        titleLabel.textColor = UI.TextColor
        excerptLabel.textColor = UI.TextColor
        fontNameLabel.textColor = UI.TextColor
        
        titleLabel.numberOfLines = 2
        excerptLabel.numberOfLines = 4
        fontNameLabel.font = UIFont.systemFontOfSize(12.0)
        fontNameLabel.textAlignment = .Right
        
        let bg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 200))
        bg.backgroundColor = UI.BaseColor
        view.addSubview(bg)
        
        view.addSubview(titleLabel)
        view.addSubview(excerptLabel)
        view.addSubview(fontNameLabel)
        
        favoriteButton.setImage(UIImage(named: "favoriteButtonOff"), forState: .Normal)
        favoriteButton.setImage(UIImage(named: "favoriteButtonOn"), forState: .Selected)
        favoriteButton.addTarget(self, action: "tapFavorite:", forControlEvents: .TouchUpInside)
        view.addSubview(favoriteButton)
    }
    
    private dynamic func tapFavorite(button:UIButton){
        favoriteButton.selected = !favoriteButton.selected
        let favorite = favoriteButton.selected
        
        font.favorite = favorite
        
        if favorite{
            Util.addFavorite(font)
        }else{
            Util.removeFavorite(font)
        }
        
        delegate.onToggleFavorite()
    }
    
    // MARK: public
    
    func reloadSelf(){
        reload(font)
    }
    
    func reload(font:Font){
        self.font = font
        let article = Util.article(font)
        
        favoriteButton.selected = font.favorite
        
        var y:CGFloat = 10
        titleLabel.frame = CGRectMake(10, y, UI.WindowWidth-20, 1000)
        titleLabel.font = font.font(40)
        if !font.adjustedLineHeight(){
            titleLabel.attributedText = UI.attributedText(UI.JaFontLineHeight, kerning: 0, text: article.title)
        }else{
            titleLabel.text = article.title
        }
        titleLabel.sizeToFit()
        y += titleLabel.frame.size.height + 10
        
        excerptLabel.frame = CGRectMake(10, y, UI.WindowWidth - 20, 1000)
        excerptLabel.font = font.font(16)
        if !font.adjustedLineHeight(){
            excerptLabel.attributedText = UI.attributedText(UI.JaFontLineHeight, kerning: 0, text: article.excerpt)
        }else{
            excerptLabel.text = article.excerpt
        }
        excerptLabel.sizeToFit()
        excerptLabel.frame.size.height = min(excerptLabel.frame.size.height, 170-10-y-10)
        y += excerptLabel.frame.size.height + 10
        
        fontNameLabel.text = font.fontName
    }
    
}
