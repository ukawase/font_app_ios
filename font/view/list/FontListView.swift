//
//  FontListView.swift
//  font
//
//  Created by kawase yu on 2015/12/25.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils
import iAd

protocol FontListViewDelegate:class{
    
    func onSelectArticle(font:Font)
    
}

class FontListView: NSObject, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate
, FontPreviewDelegate, FontCellDelegate, ReviewHeaderDelegate{
    
    weak var delegate:FontListViewDelegate!
    var fontFamilyList:[FontFamily] = []
    let view = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, UI.WindowHeight))
    private let lang:FontLang!
    private let tableView = UITableView(frame: CGRectMake(0, 200, UI.WindowWidth, UI.WindowHeight-200))
    private let preview = FontPreview()
    private let tableFooterView = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, /*270*/ 120 ))
    private let adView = UIView(frame: CGRectMake((UI.WindowWidth - 320) / 2, 10, 320, 100))
    private let reviewHeader = ReviewHeader()
    
    init(lang:FontLang) {
        self.lang = lang
        
        super.init()
        
        tableView.separatorStyle = .None
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UI.MainColor
        tableView.contentInset = UIEdgeInsetsMake(20, 0, UI.WindowHeight-220-44-30-120, 0)
        view.addSubview(tableView)
        
        preview.delegate = self
        view.addSubview(preview.view)
        
        tableView.tableFooterView = tableFooterView
        
        reviewHeader.delegate = self
        
        let statusBarView = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 20))
        let statusBarBg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 20))
        statusBarBg.backgroundColor = UI.BaseColor
        statusBarBg.alpha = UI.BgOpacity
        statusBarView.addSubview(statusBarBg)
        let line = UIView(frame: CGRectMake(0, 19, UI.WindowWidth, 1))
        line.backgroundColor = UI.LineColor
        statusBarView.addSubview(line)
        view.addSubview(statusBarView)
        
    }
    
    func reload(fontFamilyList:[FontFamily]){
        self.fontFamilyList = fontFamilyList
        tableView.reloadData()
        
        currentIndexPath = NSIndexPath()
        scrollViewDidScroll(tableView)
    }
    
    func enableReview(){
        if Util.showReview(){
            tableView.tableHeaderView = reviewHeader.headerView
        }else{
            tableView.tableHeaderView = nil
        }
    }
    
    func disableReview(){
        tableView.tableHeaderView = nil
    }
    
    func toTop(){
        tableView.contentOffset.y = -20
    }
    
    func littleUp(){
        
//        self.tableView.frame.size.height = UI.WindowHeight-200+20
//        AloeTween.doTween(0.3, ease: .OutSine) { (val) -> () in
//            self.view.transform = CGAffineTransformMakeTranslation(0, -20*val)
//        }
    }
    
    func littleDown(){
        
//        AloeChain().add(0.3, ease: .OutSine) { (val) -> () in
//            self.view.transform = CGAffineTransformMakeTranslation(0, -20*(1-val))
//        }.call { () -> () in
//            self.tableView.frame.size.height = UI.WindowHeight-200
//        }.execute()
    }
    
    // MARK: private
    
    private func fontFamilyFromIndexPath(indexPath:NSIndexPath)->FontFamily{
        return fontFamilyList[indexPath.section]
    }
    
    private func familyNameFromIndexPath(indexPath:NSIndexPath)->String{
        return fontFamilyFromIndexPath(indexPath).familyName
    }
    
    private func fontFromIndexPath(indexPath:NSIndexPath)->Font{
        return fontFamilyFromIndexPath(indexPath).fontList[indexPath.row]
    }
    
    private func hideReviewSection(){
        Util.evalutionedReview()
        let headerView = reviewHeader.headerView
        let currentHeight = headerView.frame.size.height
        
        AloeChain().add(0.2, ease: .None) { (val) -> () in
            headerView.frame.size.height = currentHeight * (1-val)
            self.tableView.tableHeaderView = headerView
        }.execute()
    }
    
    // MARK: FontPreviewDelegate
    
    func onToggleFavorite() {
        scrollViewDidScroll(tableView)
        
        if let cells = tableView.visibleCells as? [FontCell]{
            for cell in cells{
                cell.reloadSelf()
            }
        }
    }
    
    // MARK: FontCellDelegate
    
    func onToggleFavoriteFromCell(){
        preview.reloadSelf()
    }
    
    // MARK: ReviewHeaderDelegate
    
    func onCancelReview() {
        hideReviewSection()
    }
    
    func onToReview() {
        hideReviewSection()
        
        let reviewUrl = "https://goo.gl/RACqpf"
        let url = NSURL(string: reviewUrl)
        if UIApplication.sharedApplication().canOpenURL(url!){
            UIApplication.sharedApplication().openURL(url!)
        }
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fontFamilyList.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fontFamilyList[section].fontList.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let font = fontFamilyList[indexPath.section].fontList[indexPath.row]
        return FontCell.height(font)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier:String = "fontCellIdentifier"
        
        var cell:FontCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! FontCell?
        
        if cell == nil {
            cell = FontCell(style:UITableViewCellStyle.Default, reuseIdentifier:cellIdentifier)
            cell!.setup()
            cell!.delegate = self
        }
        
        let font = fontFromIndexPath(indexPath)
        cell!.reload(font)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var sectionHeader = tableView.headerViewForSection(section) as! FontFamilySection?
        if sectionHeader == nil{
            sectionHeader = FontFamilySection()
            sectionHeader!.setup()
        }
        
        sectionHeader!.reload(fontFamilyList[section])
        return sectionHeader
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! FontCell
        cell.light()
        
        let font = fontFamilyFromIndexPath(indexPath).fontList[indexPath.row]
        delegate.onSelectArticle(font)
    }
    
    private var currentIndexPath = NSIndexPath()
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        var point = tableView.contentOffset
        point.y += 30 /* section */ + 50 /* padding */
        guard let indexPath = tableView.indexPathForRowAtPoint(point) else{
            
            if self.fontFamilyList.count > 0 && point.y < 100 {
                if let cells = tableView.visibleCells as? [FontCell]{
                    for cell in cells{
                        cell.blur()
                    }
                }
                
                let ip = NSIndexPath(forRow: 0, inSection: 0)
                let font = fontFamilyList[ip.section].fontList[ip.row]
                preview.reload(font)
                if let targetCell = tableView.cellForRowAtIndexPath(ip) as? FontCell{
                    targetCell.focus()
                }
            }
            
            return
        }
        
        if currentIndexPath == indexPath{
            return
        }
        
        currentIndexPath = indexPath
        
        let font = fontFamilyList[indexPath.section].fontList[indexPath.row]
        preview.reload(font)
        
        if let cells = tableView.visibleCells as? [FontCell]{
            for cell in cells{
                cell.blur()
            }
            
            
            if let targetCell = tableView.cellForRowAtIndexPath(indexPath) as? FontCell{
                targetCell.focus()
            }
        }
        
    }
    
}
