//
//  ListFooter.swift
//  font
//
//  Created by kawase yu on 2015/12/28.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

protocol ListFooterDelegate:class{
    
    func toAll()
    func toFavorite()
    func toSearch(keyword:String)
    func onShowInfo()
    func onHideInfo()
    func onChangeSetting()
    
}

class ListFooter: NSObject, UITextFieldDelegate
, FooterButtonDelegate, FontInfoDelegate, FontSettingDelegate{
    
    weak var delegate:ListFooterDelegate!
    let view = UIView(frame: CGRectMake(0, UI.WindowHeight-44, UI.WindowWidth, 44))
    private let activeBg = UIView(frame: CGRectMake(0, 0, 54 ,44))
    private let buttonList:[FooterButton]
    private var currentButtonIndex:Int = 0
    private let tf = UITextField(frame: CGRectMake(5, 0, UI.WindowWidth-(54*3)-10-10, 34))
    private let tfWrapper = UIView(frame: CGRectMake(54*3, 5, UI.WindowWidth-(54*3)-10, 34))
    private let blackLayer = UIView(frame: AloeDevice.windowFrame())
    private var parentView:UIView!
    private let info = FontInfo()
    private let setting = FontSetting()
    private var _currentIndex:Int = 0
    
    override init() {
        
        let bg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 44))
        bg.backgroundColor = UI.BaseColor
        bg.alpha = UI.BgOpacity
        view.addSubview(bg)
        
        activeBg.backgroundColor = UI.AccentColor
        view.addSubview(activeBg)
        
        let line = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 1))
        line.backgroundColor = UI.LineColor
        view.addSubview(line)
        
        // button
        let allButton = FooterButton(index: 0, image: UIImage(named: "all")!, activeImage: UIImage(named: "allOn")!)
        view.addSubview(allButton.view)
        
        let favoriteButton = FooterButton(index: 1, image: UIImage(named: "favorite")!, activeImage:UIImage(named: "favoriteOn")!)
        view.addSubview(favoriteButton.view)
        
        let searchButton = FooterButton(index: 2, image: UIImage(named: "search")!, activeImage:UIImage(named: "searchOn")!)
        view.addSubview(searchButton.view)
        
        buttonList = [allButton, favoriteButton, searchButton]
        
        super.init()
        
        allButton.delegate = self
        favoriteButton.delegate = self
        searchButton.delegate = self
        
        // info
        setInfoButton()
        setSettingButton()
        
        // 検索TextFields
        setSearchTf()
        
        blackLayer.backgroundColor = UIColor.blackColor()
        blackLayer.alpha = 0
        blackLayer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapBlack"))
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: "handleKeyboardWillShowNotification:", name: UIKeyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: "handleKeyboardWillHideNotification:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private func setInfoButton(){
        info.delegate = self
        info.infoButton.frame.origin.x = UI.WindowWidth - 44 - 5
        view.insertSubview(info.infoButton, atIndex: 1)
    }
    
    private func setSettingButton(){
        setting.delegate = self
        setting.settingButton.frame.origin.x = UI.WindowWidth - 44 - 5 - 44
        view.insertSubview(setting.settingButton, atIndex: 1)
    }
    
    private func setSearchTf(){
        tfWrapper.backgroundColor = UI.BaseColor
        tfWrapper.clipsToBounds = true
        tfWrapper.layer.cornerRadius = 2
        tfWrapper.addSubview(tf)
        tfWrapper.hidden = true
        tf.delegate = self
        tf.autocorrectionType = .No
        tf.keyboardType = .ASCIICapable
        tf.returnKeyType = .Search
        tf.clearButtonMode = .Always
        tf.autocapitalizationType = .None
        tf.textColor = UI.TextColor
        tf.addTarget(self, action: "textFieldEditingChanged:", forControlEvents: .EditingChanged)
        
        view.addSubview(tfWrapper)
    }
    
    private dynamic func tapBlack(){
        hideBlack()
    }
    
    private func hideBlack(){
        AloeChain().add(0.2, ease: .None) { (val) -> () in
            self.blackLayer.alpha = UI.BlackLayerOpacity * (1-val)
        }.call({ () -> () in
            self.blackLayer.removeFromSuperview()
            self.parentView.addSubview(self.view)
        }).execute()
        
        tf.resignFirstResponder()
    }
    
    private func showBlack(){
        parentView = view.superview
        
        let rootView = Util.rootView()
        rootView.addSubview(blackLayer)
        rootView.addSubview(view)
        AloeChain().add(0.2, ease: .None) { (val) -> () in
            self.blackLayer.alpha = UI.BlackLayerOpacity * val
        }.call({ () -> () in
            
        }).execute()
    }
    
    private dynamic func textFieldEditingChanged(sender: UITextField) {
        
        guard let keyword = sender.text else{
            return
        }
        
        delegate.toSearch(keyword)
    }
    
    // MARK: notification
    
    //キーボードが表示された時
    private dynamic func handleKeyboardWillShowNotification(notification: NSNotification) {
        
        let userInfo = notification.userInfo!
        let keyboardRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        let keyboardHeight = keyboardRect.height
        
        let currentTy = view.transform.ty
        let toTy = -keyboardHeight
        let saTy = toTy - currentTy
        AloeTween.doTween(0.3, ease: .OutQuad) { (val) -> () in
            let ty = currentTy + (saTy * val)
            self.view.transform = CGAffineTransformMakeTranslation(0, ty)
        }
    }
    
    //ずらした分を戻す処理
    private dynamic func handleKeyboardWillHideNotification(notification: NSNotification) {
        
        let currentTy = view.transform.ty
        AloeTween.doTween(0.3, ease: .OutQuad) { (val) -> () in
            self.view.transform = CGAffineTransformMakeTranslation(0, currentTy * (1-val))
        }
    }
    
    private func focusSearch(){
        
        guard let text = tf.text else{
            AloeThread.wait(0.2, proc: { () -> () in
                self.tf.becomeFirstResponder()
            })
            return
        }
        
        if text.characters.count == 0{
            AloeThread.wait(0.2, proc: { () -> () in
                self.tf.becomeFirstResponder()
            })
            
        }else{
            delegate.toSearch(text)
        }
    }
    
    // MARK: InfoDelegate
    
    func onShowInfo(){
        delegate.onShowInfo()
    }
    
    func onHideInfo(){
        delegate.onHideInfo()
    }
    
    // MARK: FontSettingDelegate
    
    func onShowSetting() {
        delegate.onShowInfo()
    }
    
    func onHideSetting() {
        delegate.onHideInfo()
    }
    
    func onChangeSetting() {
        delegate.onChangeSetting()
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if let text = textField.text{
            delegate.toSearch(text)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        showBlack()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        hideBlack()
    }
    
    // MARK: FooterButtonDelegate
    
    func onSelectButton(index: Int) {
        
        _currentIndex = index
        
        for i in 0 ..< self.buttonList.count{
            let button = self.buttonList[i]
            if i == index{
                continue
            }
            button.deactive()
        }
        
        tfWrapper.hidden = index != 2
        
        let currentX:CGFloat = activeBg.frame.origin.x
        let toX = CGFloat(index) * 54
        let sa = toX - currentX
        
        let currentWidth = activeBg.frame.size.width
        let toWidth = (index == 2) ? (UI.WindowWidth - (54*2)) : 54
        let saWidth = toWidth - activeBg.frame.size.width
        
        var w:Double = 0
        if blackLayer.alpha != 0{
            tf.resignFirstResponder()
            w = 0.3
        }
        
        AloeChain().wait(w).call({ () -> () in
            if index == 0{
                self.delegate.toAll()
            }else if index == 1{
                self.delegate.toFavorite()
            }else if index == 2{
                self.focusSearch()
            }
        }).add(0.2, ease: .None) { (val) -> () in
            self.activeBg.frame.origin.x = currentX + (sa * val)
            self.activeBg.frame.size.width = currentWidth + (saWidth * val)
        }.execute()
    }
    
    // MARK: public
    
    func currentIndex()->Int{
        return _currentIndex
    }
    
    func searchKeyword()->String{
        if let keyword = tf.text{
            return keyword
        }
        
        return ""
    }
    
}
