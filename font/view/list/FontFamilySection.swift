//
//  FontFamilySection.swift
//  font
//
//  Created by kawase yu on 2015/12/27.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

class FontFamilySection: UITableViewHeaderFooterView {

    private var fontfamily:FontFamily!
    private let familyNameLabel = UILabel(frame: CGRectMake(10, 0, UI.WindowWidth, 30))
    private let bg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 30))
    
    func setup(){
        bg.backgroundColor = UI.MainColor
        contentView.addSubview(bg)
        
        familyNameLabel.font = UIFont.systemFontOfSize(14.0)
        familyNameLabel.textColor = UI.BaseColor
        contentView.addSubview(familyNameLabel)
    }
    
    func reload(fontfamily:FontFamily){
        self.fontfamily = fontfamily
        familyNameLabel.text = fontfamily.familyName
    }

}
