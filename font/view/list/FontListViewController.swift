//
//  FontListViewController.swift
//  font
//
//  Created by kawase yu on 2015/12/25.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

class FontListViewController: UIViewController, UIScrollViewDelegate
, FontListViewDelegate, ListFooterDelegate {
    
    weak var delegate:FontListViewDelegate!
    private let listView = FontListView(lang: .JA)
    private let footer = ListFooter()
    private var allFontFamilyList:[FontFamily] = []
    private var searchFontFamilyList:[FontFamily] = []
    
    func setup(){
        
        view.backgroundColor = UIColor.whiteColor()
        
        listView.delegate = self
        view.addSubview(listView.view)
        
        footer.delegate = self
        view.addSubview(footer.view)
        
    }
    
    func reload(){
        print("reload")
        
        updateDataList()
        listView.enableReview()
        listView.reload(allFontFamilyList)
        
        if AloeDevice.isForeignDevice(){
            return
        }
        let loading = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        loading.frame = CGRectMake(UI.WindowWidth-44, 20, 44, 44)
        loading.startAnimating()
        view.addSubview(loading)
        AloeFont.useFont(AloeFont.DOWNLOADABLE_JA_FONT_NAME_LIST) { () -> () in
            self.updateDataList()
            self.reloadViews()
            loading.stopAnimating()
        }
    }
    
    private func updateDataList(){
        var jaFontFamilyList:[FontFamily] = []
        let favoriteFontNameList = Util.favoriteFontNameList()
        
        allFontFamilyList.removeAll()
        for familyName in UIFont.familyNames(){
            var fontNameList:[String] = []
            for fontName in UIFont.fontNamesForFamilyName(familyName){
                fontNameList.append(fontName)
            }
            
            let fontFamily = FontFamily(familyName: familyName, fontNameList: fontNameList)
            fontFamily.mergeFavorite(favoriteFontNameList)
            
            if AloeFont.JA_FONT_FAMILY_LIST.contains(fontFamily.familyName) && !AloeDevice.isForeignDevice(){
                jaFontFamilyList.append(fontFamily)
                fontFamily.ja()
            }else{
                allFontFamilyList.append(fontFamily)
            }
        }
        
        allFontFamilyList.sortInPlace({ $0.familyName < $1.familyName })
        
        if !AloeDevice.isForeignDevice(){
            jaFontFamilyList.sortInPlace({ $0.familyName > $1.familyName })
            for fontFamily in jaFontFamilyList{
                allFontFamilyList.insert(fontFamily, atIndex: 0)
            }
        }
    }
    
    private func reloadViews(){
        
        var currentDataList = allFontFamilyList
        listView.enableReview()
        if footer.currentIndex() == 1{
            listView.disableReview()
            currentDataList = createFavoriteFontFamilyList()
        }else if footer.currentIndex() == 2{
            listView.disableReview()
            currentDataList = createSearchFamilyList(footer.searchKeyword())
        }
        
        AloeChain().add(0.2, ease: .None) { (val) -> () in
            self.listView.view.alpha = 1 - val
        }.call({ () -> () in
            self.listView.reload(currentDataList)
        }).add(0.2, ease: .None) { (val) -> () in
            self.listView.view.alpha = val
        }.execute()
    }
    
    private func createFavoriteFontFamilyList()->[FontFamily]{
        var list:[FontFamily] = []
        
        for fontFamily in allFontFamilyList{
            var _fontList:[Font] = []
            
            for font in fontFamily.fontList{
                if font.favorite{
                    _fontList.append(font)
                }
            }
            
            if _fontList.count == 0{
                continue
            }
            
            let _fontFamily = FontFamily(familyName: fontFamily.familyName, fontNameList: [])
            _fontFamily.reloadFontList(_fontList)
            list.append(_fontFamily)
        }
        
        return list
    }
    
    private func createSearchFamilyList(keyword:String)->[FontFamily]{
        if keyword.characters.count == 0{
            return allFontFamilyList
        }
        
        var resultList:[FontFamily] = []
        for i in 0..<allFontFamilyList.count{
            let fontFamily = allFontFamilyList[i]
            if let result = fontFamily.containKeyword(keyword){
                resultList.append(result)
            }
        }
        
        return resultList
    }
    
    // MARK: FontListViewDelegate
    
    func onSelectArticle(font: Font) {
        delegate.onSelectArticle(font)
    }
    
    // MARK: ListFooterDelegate
    
    func toAll() {
        listView.enableReview()
        listView.reload(allFontFamilyList)
        listView.toTop()
        Util.trackScreen("all")
    }
    
    func toFavorite() {
        listView.disableReview()
        listView.reload(createFavoriteFontFamilyList())
        listView.toTop()
        
        Util.trackScreen("favorite")
    }
    
    func toSearch(keyword:String) {
        listView.disableReview()
        
        let resultList = createSearchFamilyList(keyword)
        listView.reload(resultList)
        listView.toTop()
        
        Util.trackScreen("search")
        Util.trackEvent("検索", action: keyword)
    }
    
    func onHideInfo() {
        
        AloeChain().add(0.3, ease: .InSine) { (val) -> () in
            let scale = 0.97 + (0.03*val)
            self.view.transform = CGAffineTransformMakeScale(scale, scale)
        }.call { () -> () in
            Util.rootView().backgroundColor = UIColor.whiteColor()
        }.execute()
//        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .Slide)
//        listView.littleDown()
    }
    
    func onShowInfo() {
        Util.rootView().backgroundColor = UIColor.blackColor()
        AloeTween.doTween(0.3, ease: .OutSine) { (val) -> () in
            let scale = 1.00 - (0.03 * val)
            self.view.transform = CGAffineTransformMakeScale(scale, scale)
        }
        
//        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .Slide)
//        listView.littleUp()
    }
    
    func onChangeSetting() {
        self.reloadViews()
    }
    
    // MARK: default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
    }

}
