//
//  ReviewHeader.swift
//  font
//
//  Created by kawase yu on 2016/01/03.
//  Copyright © 2016年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

protocol ReviewHeaderDelegate:class{
    
    func onCancelReview()
    func onToReview()
    
}

class ReviewHeader: NSObject {

    weak var delegate:ReviewHeaderDelegate!
    let headerView = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 124))
    private let cancelButton = UILabel()
    private let reviewButton = UILabel()
    
    override init() {
        super.init()
        headerView.clipsToBounds = true
        headerView.backgroundColor = UIColor.whiteColor()
        
        let section = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 30))
        section.backgroundColor = UI.MainColor
        headerView.addSubview(section)
        let sectionLabel = UILabel(frame: CGRectMake(10, 0, UI.WindowWidth-20, 30))
        sectionLabel.font = UIFont.systemFontOfSize(14.0)
        sectionLabel.textColor = UI.BaseColor
        sectionLabel.text = Word.experience()
        section.addSubview(sectionLabel)
        
        let cell = UIView(frame: CGRectMake(0, 30, UI.WindowWidth, 94))
        headerView.addSubview(cell)
        
        let label = UILabel(frame: CGRectMake(10, 10, UI.WindowWidth-20, 20))
        label.font = UIFont.systemFontOfSize(14.0)
        label.textColor = UI.TextColor
        label.text = Word.pleaseReview()
        cell.addSubview(label)
        
        let buttonWidth:CGFloat = (UI.WindowWidth - 30) / 2
        cancelButton.frame = CGRectMake(10, 40, buttonWidth, 44)
        cancelButton.backgroundColor = UI.MainColor
        cancelButton.clipsToBounds = true
        cancelButton.layer.cornerRadius = UI.ButtonR
        cancelButton.textColor = UI.BaseColor
        cancelButton.font = UIFont.systemFontOfSize(16)
        cancelButton.textAlignment = .Center
        cancelButton.text = Word.cancelReview()
        cancelButton.userInteractionEnabled = true
        cancelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapCancel"))
        cell.addSubview(cancelButton)
        
        reviewButton.frame = CGRectMake((UI.WindowWidth / 2) + 5, 40, buttonWidth, 44)
        reviewButton.backgroundColor = UI.AccentColor
        reviewButton.clipsToBounds = true
        reviewButton.layer.cornerRadius = UI.ButtonR
        reviewButton.textColor = UI.BaseColor
        reviewButton.font = UIFont.systemFontOfSize(16)
        reviewButton.textAlignment = .Center
        reviewButton.text = Word.writeReview()
        reviewButton.userInteractionEnabled = true
        reviewButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapReview"))
        cell.addSubview(reviewButton)
    }
    
    private dynamic func tapCancel(){
        AloeChain().add(0.1, ease: .None) { (val) -> () in
            let scale = 1 - (0.03 * val)
            self.cancelButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.add(0.1, ease: .OutBack) { (val) -> () in
            let scale = 0.97 + (0.03 * val)
            self.cancelButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.call({ () -> () in
            self.delegate.onCancelReview()
        }).execute()
    }
    
    private dynamic func tapReview(){
        AloeChain().add(0.1, ease: .None) { (val) -> () in
            let scale = 1 - (0.03 * val)
            self.reviewButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.add(0.1, ease: .OutBack) { (val) -> () in
            let scale = 0.97 + (0.03 * val)
            self.reviewButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.call({ () -> () in
            self.delegate.onToReview()
        }).execute()
    }
    
}
