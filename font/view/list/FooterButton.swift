//
//  FooterButton.swift
//  font
//
//  Created by kawase yu on 2016/01/02.
//  Copyright © 2016年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

protocol FooterButtonDelegate:class{
    
    func onSelectButton(index:Int)
    
}


class FooterButton: NSObject {

    weak var delegate:FooterButtonDelegate!
    let view = UIView()
    
    private let index:Int
    private let imageView = UIImageView(frame: CGRectMake(5, 0, 44, 44))
    private let activeImageView = UIImageView(frame: CGRectMake(5, 0, 44, 44))
    
    init(index:Int, image:UIImage, activeImage:UIImage){
        self.index = index
        super.init()
        
        view.frame = CGRectMake(CGFloat(54*index), 0, 54, 44)
        
        imageView.image = image
        activeImageView.image = activeImage
        view.addSubview(imageView)
        view.addSubview(activeImageView)
        
        activeImageView.alpha = 0
        if index == 0{
            activeImageView.alpha = 1
            imageView.alpha = 0
        }
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tap"))
    }
    
    private dynamic func tap(){
        if isActive(){
            return
        }
        
        delegate.onSelectButton(index)
        active()
    }
    
    private func isActive()->Bool{
        return activeImageView.alpha != 0
    }
    
    private func active(){
        AloeTween.doTween(0.2, ease: .None) { (val) -> () in
            self.activeImageView.alpha = val
            self.imageView.alpha = 1-val
        }
    }
    
    func deactive(){
        if !isActive(){
            return
        }
        AloeTween.doTween(0.2, ease: .None) { (val) -> () in
            self.activeImageView.alpha = 1-val
            self.imageView.alpha = val
        }
    }

    
}
