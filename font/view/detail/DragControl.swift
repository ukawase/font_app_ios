//
//  DragControl.swift
//  font
//
//  Created by kawase yu on 2016/01/01.
//  Copyright © 2016年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

protocol DragControlDelegate: class{
    
    func onBeganKerning()
    func onBeganLineheight()
    
    func onChangeKerning(kerningValue:CGFloat)
    func onChangeLineheight(lineheight:CGFloat)
    
    func onEndKerning()
    func onEndLineheight()
    
}

class DragControl: NSObject {

    weak var delegate:DragControlDelegate!
    private let targetView:UIView
    private let KerningStep:CGFloat = 0.1
    private var kerningValue:CGFloat = 0
    private let LineheightStep:CGFloat = 0.01
    private var lineheightValue:CGFloat = 1.0
    
    private let valueView = UIView(frame: CGRectMake(0, 0, 80, 40))
    private let valueLabel = UILabel(frame: CGRectMake(0, 0, 80, 40))
    
    init(targetView:UIView) {
        self.targetView = targetView
        super.init()
        targetView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: "onPan:"))
        
        valueView.alpha = 0
        
        let valueBg = UIView(frame: CGRectMake(0, 0, 80, 40))
        valueBg.backgroundColor = UI.LightColor
        valueBg.clipsToBounds = true
        valueBg.layer.cornerRadius = UI.ButtonR
        valueView.addSubview(valueBg)
        
        valueLabel.textColor = UI.BaseColor
        valueLabel.textAlignment = .Center
        valueLabel.font = UIFont.systemFontOfSize(15.0)
        valueView.addSubview(valueLabel)
        
        targetView.addSubview(valueView)
    }
    
    // MARK: public
    
    func setLineheight(val:CGFloat){
        lineheightValue = val
    }
    
    // MARK: private
    
    private var movePoint = CGPointMake(0, 0)
    private var moveX:Bool = false
    
    private dynamic func onPan(sender:UIPanGestureRecognizer){
        
        let view = sender.view!
        let state = sender.state
        
        if state == .Ended || state == .Cancelled{
            endDrag()
            return
        }
        
        if state == .Began{
            movePoint = CGPointMake(0, 0)
            let velocity = sender.velocityInView(view)
            moveX = (abs(velocity.x) > abs(velocity.y))
            let location = sender.locationInView(view)
            
            valueView.transform = CGAffineTransformMakeTranslation(0, 0)
            if moveX{
                valueView.frame.origin.x = location.x - 40
                valueView.frame.origin.y = location.y - 80
            }else{
                if location.x < UI.WindowWidth/2{
                    valueView.frame.origin.x = location.x + 50
                }else{
                    valueView.frame.origin.x = location.x - 130
                }
                valueView.frame.origin.y = location.y - 20
            }
            
            startDrag()
            return
        }
        
        let translate = sender.translationInView(view)
        sender.setTranslation(CGPointZero, inView: view)
        
        if moveX{
            let tx = valueView.transform.tx + translate.x
            valueView.transform = CGAffineTransformMakeTranslation(tx, 0)
        }else{
            let ty = valueView.transform.ty + translate.y
            valueView.transform = CGAffineTransformMakeTranslation(0, ty)
        }
        
        movePoint.x += translate.x
        movePoint.y += translate.y
        
        // kerning
        if moveX{
            if movePoint.x > 20{
                kerningValue += KerningStep
                update()
                movePoint.x = 0
            }else if movePoint.x < -20{
                kerningValue -= KerningStep
                update()
                movePoint.x = 0
            }
        }
        // lineHeight
        else{
            if movePoint.y > 20{
                lineheightValue += LineheightStep
                update()
                movePoint.y = 0
            }else if movePoint.y < -20{
                lineheightValue -= LineheightStep
                update()
                movePoint.y = 0
            }
        }
    }
    
    private func update(){
        if moveX{
            valueLabel.text = String(format: "%.01f", Float(kerningValue))
            delegate.onChangeKerning(kerningValue)
        }else{
            valueLabel.text = String(format: "%.02f", Float(lineheightValue))
            delegate.onChangeLineheight(lineheightValue)
        }
    }
    
    private func endDrag(){
        if moveX{
            delegate.onEndKerning()
        }else{
            delegate.onEndLineheight()
        }
        
        AloeChain().add(0.2, ease: .OutExpo) { (val) -> () in
            self.valueView.alpha = 1 - val
        }.execute()
    }
    
    private func startDrag(){
        if moveX{
            delegate.onBeganKerning()
        }else{
            delegate.onBeganLineheight()
        }
        
        AloeChain().add(0.2, ease: .None) { (val) -> () in
            self.valueView.alpha = val
        }.execute()
    }
}
