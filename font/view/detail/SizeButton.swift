//
//  SizeButton.swift
//  font
//
//  Created by kawase yu on 2016/01/01.
//  Copyright © 2016年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

protocol SizeButtonDelegate:class{
    
    func onBeganSize()
    func onChangeSize(size:CGFloat)
    func onEndSize()
}

class SizeButton: NSObject {

    weak var delegate:SizeButtonDelegate!
    let view = UIView(frame: CGRectMake(UI.WindowWidth-88-10-10, UI.WindowHeight-44-44-10, 88+20, 44))
    
    private let plusButton = UIButton(frame: CGRectMake(54, 0, 44, 44))
    private let plusBg = UIView(frame: CGRectMake(0, 0, 44, 44))
    
    private let minusButton = UIButton(frame: CGRectMake(0, 0, 44, 44))
    private let minusBg = UIView(frame: CGRectMake(0, 0, 44, 44))
    private var timer:NSTimer?
    
    private let valueView = UIView(frame: CGRectMake(9, -50, 80, 40))
    private let valueLabel = UILabel(frame: CGRectMake(0, 0, 80, 40))
    
    private var currentValue:CGFloat = 16.0
    private let DefaultValue:CGFloat = 16.0
    private let Step:CGFloat = 1.0
    
    private var targetLarge:Bool = false
    
    override init() {
        super.init()
        
        if AloeDevice.isForeignDevice(){
            view.frame.origin.y -= 50
        }
        
        view.addSubview(plusButton)
        plusBg.backgroundColor = UI.MainColor
        plusBg.layer.cornerRadius = 22
        plusBg.clipsToBounds = true
        plusBg.userInteractionEnabled = false
        plusButton.addSubview(plusBg)
        plusButton.addTarget(self, action: "tapPlus", forControlEvents: .TouchUpInside)
        
        let plusImageView = UIImageView(image: UIImage(named: "sizePlus"))
        plusImageView.frame = CGRectMake(0, 0, 44, 44)
        plusImageView.userInteractionEnabled = false
        plusButton.addSubview(plusImageView)
        
        view.addSubview(minusButton)
        minusBg.backgroundColor = UI.MainColor
        minusBg.layer.cornerRadius = 22
        minusBg.clipsToBounds = true
        minusBg.userInteractionEnabled = false
        minusButton.addSubview(minusBg)
        minusButton.addTarget(self, action: "tapMinus", forControlEvents: .TouchUpInside)
        
        let minusImageView = UIImageView(image: UIImage(named: "sizeMinus"))
        minusImageView.frame = CGRectMake(0, 0, 44, 44)
        minusImageView.userInteractionEnabled = false
        minusButton.addSubview(minusImageView)
        
        valueView.alpha = 0
        view.addSubview(valueView)
        let valueBg = UIView(frame: CGRectMake(0, 0, 80, 40))
        valueBg.backgroundColor = UI.AccentColor
        valueBg.clipsToBounds = true
        valueBg.layer.cornerRadius = UI.ButtonR
        valueView.addSubview(valueBg)
        
        valueLabel.textColor = UI.BaseColor
        valueLabel.textAlignment = .Center
        valueLabel.font = UIFont.systemFontOfSize(15.0)
        valueView.addSubview(valueLabel)
    }
    
    private dynamic func tapPlus(){
        currentValue += Step
        valueLabel.text = String(format: "%.00f", Float(currentValue))
        delegate.onChangeSize(currentValue)
        
        plusBg.backgroundColor = UI.AccentColor
        minusBg.backgroundColor = UI.MainColor
        
        AloeChain().add(0.1, ease: .None) { (val) -> () in
            let scale:CGFloat = 1.0 - (0.05 * val)
            self.plusButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.add(0.1, ease: .OutBack) { (val) -> () in
            let scale:CGFloat = 0.95 + (0.05 * val)
            self.plusButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.execute()
        
        startTimer()
    }
    
    private dynamic func tapMinus(){
        currentValue -= Step
        valueLabel.text = String(format: "%.00f", Float(currentValue))
        delegate.onChangeSize(currentValue)
        
        plusBg.backgroundColor = UI.MainColor
        minusBg.backgroundColor = UI.AccentColor
        
        AloeChain().add(0.1, ease: .None) { (val) -> () in
            let scale:CGFloat = 1.0 - (0.05 * val)
            self.minusButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.add(0.1, ease: .OutBack) { (val) -> () in
            let scale:CGFloat = 0.95 + (0.05 * val)
            self.minusButton.transform = CGAffineTransformMakeScale(scale, scale)
        }.execute()
        
        startTimer()
    }
    
    private dynamic func onTime(){
        AloeTween.doTween(0.1, ease: .None) { (val) -> () in
            self.valueView.alpha = 1 - val
            self.valueView.transform = CGAffineTransformMakeTranslation(0, -10 * val)
        }
        plusBg.backgroundColor = UI.MainColor
        minusBg.backgroundColor = UI.MainColor
        delegate.onEndSize()
    }
    
    private func startTimer(){
        if valueView.alpha == 0{
            delegate.onBeganSize()
            AloeTween.doTween(0.2, ease: .None) { (val) -> () in
                self.valueView.alpha = val
                self.valueView.transform = CGAffineTransformMakeTranslation(0, 10 * (1-val))
            }
        }
        
        stopTimer()
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "onTime", userInfo: nil, repeats: false)
    }
    
    private func stopTimer(){
        if let t = timer{
            if t.valid{
                t.invalidate()
                timer = nil
            }
        }
    }
    
    func toggle(){
        if view.alpha == 0{
            show()
        }else{
            hide()
        }
    }
    
    func show(){
        AloeTween.doTween(0.2, ease: .None) { (val) -> () in
            self.view.alpha = val
        }
    }
    
    func hide(){
        AloeTween.doTween(0.2, ease: .None) { (val) -> () in
            self.view.alpha = 1 - val
        }
    }
    
}
