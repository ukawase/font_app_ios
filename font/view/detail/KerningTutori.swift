//
//  KerningTutori.swift
//  font
//
//  Created by kawase yu on 2016/01/02.
//  Copyright © 2016年 kawase. All rights reserved.
//

import UIKit
import AloeUtils

class KerningTutori: NSObject {

    let view = UIView(frame: CGRectMake((UI.WindowWidth-200) / 2, UI.WindowHeight-44-44-44-10-10, 200, 44))
    private let touchContainer = UIView(frame: CGRectMake(0, 0, 44, 44))
    private let touchView = UIView(frame: CGRectMake(0, 0, 44, 44))
    private let touchBg = UIView(frame: CGRectMake(0, 0, 44, 44))
    private let touchActiveBg = UIView(frame: CGRectMake(0, 0, 44, 44))
    private var ended:Bool = false
    
    override init() {
        view.hidden = true
        view.userInteractionEnabled = false
        
        view.addSubview(touchContainer)
        
        touchView.layer.cornerRadius = 22
        touchView.layer.borderWidth = 1
        touchView.clipsToBounds = true
        touchView.layer.borderColor = UI.MainColor.CGColor
        touchContainer.addSubview(touchView)
        
        touchBg.backgroundColor = UI.BaseColor
        touchBg.alpha = 0.9
        touchView.addSubview(touchBg)
        
        touchActiveBg.backgroundColor = UI.AccentColor
        touchActiveBg.alpha = 0
        touchView.addSubview(touchActiveBg)
        
        super.init()
    }
    
    private func tryRestart(){
        if ended {
            return
        }
        
        start()
    }
    
    // MARK: public
    
    func end(){
        ended = true
        
        AloeChain().wait(0.2).add(0.2, ease: .None) { (val) -> () in
            self.view.alpha = 1 - val
        }.call({ () -> () in
            self.view.removeFromSuperview()
        }).execute()
    }
    
    func start(){
        view.hidden = false
        
        AloeChain().wait(1.0)
            
            // 行き
            .add(0.1, ease: .None) { (val) -> () in
                self.touchActiveBg.alpha = val
                let scale = 1.0 - (0.1*val)
                self.touchView.transform = CGAffineTransformMakeScale(scale, scale)
            }.wait(0.5).add(0.5, ease: .InOutSine) { (val) -> () in
                self.touchContainer.transform = CGAffineTransformMakeTranslation(156*val, 0)
            }.wait(0.5).add(0.1, ease: .None, progress: { (val) -> () in
                self.touchActiveBg.alpha = 1 - val
                let scale:CGFloat = 0.9 + (0.1*val)
                self.touchView.transform = CGAffineTransformMakeScale(scale, scale)
            })
            
            // 帰り
            .wait(1.0)
            .add(0.1, ease: .None, progress: { (val) -> () in
                self.touchActiveBg.alpha = val
                let scale = 1.0 - (0.1*val)
                self.touchView.transform = CGAffineTransformMakeScale(scale, scale)
            }).wait(0.5).add(0.5, ease: .InOutSine, progress: { (val) -> () in
                self.touchContainer.transform = CGAffineTransformMakeTranslation(156 * (1-val), 0)
            }).wait(0.5).add(0.1, ease: .None, progress: { (val) -> () in
                self.touchActiveBg.alpha = 1 - val
                let scale:CGFloat = 0.9 + (0.1*val)
                self.touchView.transform = CGAffineTransformMakeScale(scale, scale)
            })
            
            // 往復？
            .call({ () -> () in
                self.tryRestart()
            })
            .execute()
    }
    
    deinit{
        print("kerningTutori deinit")
    }
    
}
