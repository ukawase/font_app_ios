//
//  DetailViewController.swift
//  font
//
//  Created by kawase yu on 2015/12/26.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit
import AloeUtils
import Social
import Parse

protocol DetailViewDelegate:class{
    
}

class DetailViewController: UIViewController, UIScrollViewDelegate
, DragControlDelegate, SizeButtonDelegate{

    weak var delegate:DetailViewDelegate!
    private var font:Font!
    private var article:Article!
    
    private let menuView = UIView(frame: CGRectMake(0, UI.WindowHeight-44, UI.WindowWidth, 44))
    
    private let contentWrapper = UIView(frame: CGRectMake(0, 20, UI.WindowWidth, UI.WindowHeight-20-44))
    private let contentView = UIView()
    private let contentLabel = UILabel()
    
    private var dragControl:DragControl!
    private let sizeButton = SizeButton()
    
    private var currentFontSize:CGFloat = 16.0
    private var currentLineHeight:CGFloat = 1.0
    private var currentKerning:CGFloat = 0
    
    private let infoView = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 20))
    private let infoFontLabel = UILabel()
    private let infoSizeLabel = UILabel()
    private let infoLineheightLabel = UILabel()
    private let infoKerningLabel = UILabel()
    
    private var kerningTutori:KerningTutori?
    
    func setup(font:Font){
        // initialize
        self.font = font
        self.article = Util.article(font)
        
        view.backgroundColor = UIColor.whiteColor()
        
        // contentView
        contentWrapper.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapContent"))
        view.addSubview(contentWrapper)
        contentView.clipsToBounds = true
        contentWrapper.addSubview(contentView)
        
        // content
        contentLabel.textColor = UIColor.blackColor()
        contentLabel.userInteractionEnabled = false
        contentView.addSubview(contentLabel)
        
        // drag
        dragControl = DragControl(targetView: contentWrapper)
        dragControl.delegate = self
        
        // size
        sizeButton.delegate = self
        view.addSubview(sizeButton.view)
        
        // tutori
        if !Util.isTutoriEnd(.Kerning){
            kerningTutori = KerningTutori()
            view.addSubview(kerningTutori!.view)
            kerningTutori!.start()
            sizeButton.hide()
        }
        
        // 上下
        setHeader()
        setFooter()
        
        // info
        setInfo()
        
        if !font.adjustedLineHeight(){
            currentLineHeight = UI.JaFontLineHeight
            dragControl.setLineheight(currentLineHeight)
        }
        
        // reload
        updateLabel()
        
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .Slide)
    }
    
    dynamic private func tapContent(){
        print("tapContent")
        sizeButton.toggle()
    }
    
    private func setInfo(){
        infoView.frame  = CGRectMake(0, 0, UI.WindowWidth, 20)
        view.addSubview(infoView)
        
        var x:CGFloat = 10
        
        infoSizeLabel.frame = CGRectMake(x, 0, 43, 20)
        infoSizeLabel.textColor = UI.TextColor
        infoSizeLabel.font = UIFont.systemFontOfSize(10.0)
        infoView.addSubview(infoSizeLabel)
        x += 39 + 10
        
        infoLineheightLabel.frame = CGRectMake(x, 0, 65, 20)
        infoLineheightLabel.textColor = UI.TextColor
        infoLineheightLabel.textAlignment = .Center
        infoLineheightLabel.font = UIFont.systemFontOfSize(10.0)
        infoView.addSubview(infoLineheightLabel)
        x += 65 + 5
        
        infoKerningLabel.frame = CGRectMake(x, 0, 70, 20)
        infoKerningLabel.textColor = UI.TextColor
        infoKerningLabel.textAlignment = .Center
        infoKerningLabel.font = UIFont.systemFontOfSize(10.0)
        infoView.addSubview(infoKerningLabel)
        
        let infoPaddingLabel = UILabel(frame: CGRectMake(UI.WindowWidth-200-10, 0, 200, 20))
        infoPaddingLabel.textColor = UI.TextColor
        infoPaddingLabel.textAlignment = .Right
        infoPaddingLabel.font = UIFont.systemFontOfSize(10.0)
        infoPaddingLabel.text = "padding:\(Util.userSettingInt(.Padding))"
        infoView.addSubview(infoPaddingLabel)
    }
    
    private func updateLabel(){
        
        let padding:CGFloat = CGFloat(Util.userSettingInt(.Padding))
        let height:CGFloat = UI.WindowHeight-20-44-(padding*2)
        contentLabel.frame = CGRectMake(0, 0, UI.WindowWidth-(padding*2), height)
        contentLabel.numberOfLines = 0
        contentLabel.font = font.font(currentFontSize)
        contentLabel.attributedText = UI.attributedText(currentLineHeight, kerning: currentKerning, text: article.content)
        contentLabel.sizeToFit()
        contentView.frame = CGRectMake(padding, padding, UI.WindowWidth-(padding*2), height)
        
        infoSizeLabel.text = String(format: "size:%.00f", Float(currentFontSize))
        infoLineheightLabel.text = String(format: "height:%.02f", Float(currentLineHeight))
        infoKerningLabel.text = String(format: "kerning:%.01f", Float(currentKerning))
    }
    
    private func setHeader(){
        let header = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 20))
        let bg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 20))
        bg.backgroundColor = UI.BaseColor
        bg.alpha = UI.BgOpacity
        header.addSubview(bg)
        
        let line = UIView(frame: CGRectMake(0, 20, UI.WindowWidth, 1))
        line.backgroundColor = UI.LineColor
        header.addSubview(line)
        
        view.addSubview(header)
    }
    
    private func setFooter(){
        let footer = UIView(frame: CGRectMake(0, UI.WindowHeight-44, UI.WindowWidth, 44))
        let bg = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 44))
        bg.backgroundColor = UI.BaseColor
        bg.alpha = UI.BgOpacity
        footer.addSubview(bg)
        
        let titleLabel = UILabel(frame: CGRectMake(44, 00, UI.WindowWidth-88, 44))
        titleLabel.textAlignment = .Center
//        titleLabel.font = font.font(18.0)
        titleLabel.textColor = UI.TextColor
        titleLabel.font = UIFont.systemFontOfSize(18.0)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.text = font.fontName
        footer.addSubview(titleLabel)
        
        let backButton = UIButton(frame: CGRectMake(0, 0, 44, 44))
        backButton.setImage(UIImage(named: "backButton"), forState: .Normal)
        backButton.addTarget(self, action: "tapBack", forControlEvents: .TouchUpInside)
        footer.addSubview(backButton)
        
        let shareButton = UIButton(frame: CGRectMake(UI.WindowWidth-44, 0, 44, 44))
        shareButton.setImage(UIImage(named: "shareButton"), forState: .Normal)
        shareButton.addTarget(self, action: "tapShare", forControlEvents: .TouchUpInside)
        footer.addSubview(shareButton)
        
        let line = UIView(frame: CGRectMake(0, 0, UI.WindowWidth, 1))
        line.backgroundColor = UI.LineColor
        footer.addSubview(line)
        
        view.addSubview(footer)
    }
    
    private func setupParamLabel(label:UILabel){
        label.textAlignment = .Center
        label.textColor = UI.TextColor
        label.font = UIFont.systemFontOfSize(14.0)
    }
    
    private func plusTapped(){
        print("plusTapped")
    }
    
    dynamic private func tapBack(){
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .Slide)
        navigationController?.popViewControllerAnimated(true)
    }
    
    dynamic private func tapShare(){
        
        let info = "font-name: \(font.fontName)\nfont-family: \(font.familyName)\n\(infoSizeLabel.text!)\n\(infoLineheightLabel.text!)\n\(infoKerningLabel.text!)\n\nios app TypoLab\n"
        
        let excludedActivityTypes = [
            UIActivityTypePostToWeibo,
            UIActivityTypePostToFacebook,
            UIActivityTypeSaveToCameraRoll,
            UIActivityTypePrint,
            UIActivityTypeAirDrop,
            UIActivityTypeAssignToContact,
            UIActivityTypeAddToReadingList
        ]
        
        let activityVC = UIActivityViewController(activityItems: [info, NSURL(string: "https://goo.gl/RACqpf")!], applicationActivities: nil)
        activityVC.excludedActivityTypes = excludedActivityTypes
        
        presentViewController(activityVC, animated: true, completion: nil)
    }
    
    private func startChange(targetLabel:UILabel){
        AloeChain().add(0.1, ease: .None) { (val) -> () in
            targetLabel.transform = CGAffineTransformMakeTranslation(0, -5*val)
            targetLabel.alpha = 1 - val
        }.call({ () -> () in
            targetLabel.textColor = UI.AccentColor
            targetLabel.font = UIFont.boldSystemFontOfSize(11.0)
        }).add(0.1, ease: .None) { (val) -> () in
            targetLabel.transform = CGAffineTransformMakeTranslation(0, 5 * (1-val))
            targetLabel.alpha = val
        }.execute()
    }
    
    private func endChange(targetLabel:UILabel){
        AloeChain().add(0.1, ease: .None) { (val) -> () in
            targetLabel.transform = CGAffineTransformMakeTranslation(0, -5*val)
            targetLabel.alpha = 1 - val
        }.call({ () -> () in
            targetLabel.textColor = UI.TextColor
            targetLabel.font = UIFont.systemFontOfSize(10.0)
        }).add(0.1, ease: .None) { (val) -> () in
            targetLabel.transform = CGAffineTransformMakeTranslation(0, 5 * (1-val))
            targetLabel.alpha = val
        }.execute()
    }
    
    // MARK: DragControlDelegate
    
    func onBeganKerning() {
        startChange(infoKerningLabel)
    }
    
    func onBeganLineheight() {
        startChange(infoLineheightLabel)
    }
    
    func onChangeKerning(kerningValue: CGFloat) {
        currentKerning = kerningValue
        updateLabel()
    }
    
    func onChangeLineheight(lineheight: CGFloat) {
        currentLineHeight = lineheight
        updateLabel()
    }
    
    func onEndKerning() {
        endChange(infoKerningLabel)
        
        if let kt = kerningTutori{
            kt.end()
            kerningTutori = nil
            Util.endTutori(.Kerning)
            
            sizeButton.show()
        }
    }
    
    func onEndLineheight() {
        endChange(infoLineheightLabel)
    }
    
    // MARK: SizeBarDelegate
    
    func onChangeSize(size: CGFloat) {
        currentFontSize = size
        updateLabel()
    }
    
    func onBeganSize(){
        startChange(infoSizeLabel)
    }
    
    func onEndSize(){
        endChange(infoSizeLabel)
    }
    
    // MARK: default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    deinit{
        if let kt = kerningTutori{
            kt.end()
        }
        
//        print("detialView deinit")
    }
    
}
