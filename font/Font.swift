//
//  Font.swift
//  font
//
//  Created by kawase yu on 2015/12/26.
//  Copyright © 2015年 kawase. All rights reserved.
//

import UIKit

enum FontLang{
    case JA, EN
}

class Font: NSObject {

    private static var pool = Dictionary<String, UIFont>()
    
    let fontName:String
    let familyName:String
    var lang = FontLang.EN
    var favorite:Bool = false
    
    init(fontName:String, familyName:String) {
        self.fontName = fontName
        self.familyName = familyName
        super.init()
    }
    
    func font(size:CGFloat)->UIFont{
        
        let key = "\(fontName)_\(size)"
        if let f = Font.pool[key]{
            return f
        }
        
        var font = UIFont(name: fontName, size: size)
        if font == nil{
            print("no font!! \(fontName)")
            font = UIFont.systemFontOfSize(size)
        }else{
            Font.pool[key] = font
        }
        
        return font!
    }
    
    func adjustedLineHeight()->Bool{
        if lang == .EN{
            return true
        }
        
        if familyName.hasPrefix("M+") || familyName.hasPrefix("Rounded M+") || familyName.hasPrefix("Noto") || familyName.hasPrefix("Noteworthy"){
            return true
        }
        
        return false
    }
    
}
